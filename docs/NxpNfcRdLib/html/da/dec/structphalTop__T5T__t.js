var structphalTop__T5T__t =
[
    [ "pAlI15693DataParams", "da/dec/structphalTop__T5T__t.html#a92f532872675980fbf48657780318224", null ],
    [ "bRwa", "da/dec/structphalTop__T5T__t.html#aa4d887147dd1134e38c437107b38b190", null ],
    [ "bTerminatorTlvPresence", "da/dec/structphalTop__T5T__t.html#aca70e25407302090f72a423fc1ebb126", null ],
    [ "bMbRead", "da/dec/structphalTop__T5T__t.html#a1460e1d74c00bf6412deedf2a987fca0", null ],
    [ "bLockBlock", "da/dec/structphalTop__T5T__t.html#a2a8578a5d857db47ac533cd7dece715b", null ],
    [ "bSplFrm", "da/dec/structphalTop__T5T__t.html#aa7b2ce069ced5b5234370a6237724dbc", null ],
    [ "bExtendedCommandSupport", "da/dec/structphalTop__T5T__t.html#ab9a994b91cda6e2c14ad6c13e5970456", null ],
    [ "bOptionFlag", "da/dec/structphalTop__T5T__t.html#a7bdfc4578018f3de74d304a8da04e1a5", null ],
    [ "wMlen", "da/dec/structphalTop__T5T__t.html#a02a1c82a311f0b8e72e0cc0c3f06f30f", null ],
    [ "wNdefHeaderAddr", "da/dec/structphalTop__T5T__t.html#a4ffad20ab0fd67093576f0f520e60cfa", null ],
    [ "wNdefMsgAddr", "da/dec/structphalTop__T5T__t.html#a9ff455eb842b3f6ec4891c6555b885df", null ],
    [ "bBlockSize", "da/dec/structphalTop__T5T__t.html#a875a8fbaf9874540f83e9a6045547912", null ],
    [ "bMaxBlockNum", "da/dec/structphalTop__T5T__t.html#aa6e71bbe1c3f806590083b1653448b23", null ]
];
var group__phalMfdfEVx__Miscellaneous =
[
    [ "PHAL_MFDFEVX_ADDITIONAL_INFO", "d1/d48/group__phalMfdfEVx__Miscellaneous.html#gabefa930ca2e36e287845d56e697949f8", null ],
    [ "PHAL_MFDFEVX_WRAPPED_MODE", "d1/d48/group__phalMfdfEVx__Miscellaneous.html#gaf3d82a290289a9e8ffbb019489ae7744", null ],
    [ "PHAL_MFDFEVX_SHORT_LENGTH_APDU", "d1/d48/group__phalMfdfEVx__Miscellaneous.html#ga4064de3a70035c8496ab6775dd5d7551", null ],
    [ "phalMfdfEVx_GetConfig", "d1/d48/group__phalMfdfEVx__Miscellaneous.html#ga672128050e66fc24dc3c9f959e60faed", null ],
    [ "phalMfdfEVx_SetConfig", "d1/d48/group__phalMfdfEVx__Miscellaneous.html#ga3b58fbe416cad05b4191ae1457b4f64c", null ],
    [ "phalMfdfEVx_ResetAuthentication", "d1/d48/group__phalMfdfEVx__Miscellaneous.html#ga32adeee5e54890174446363a4bb4cc3c", null ],
    [ "phalMfdfEVx_SetVCAParams", "d1/d48/group__phalMfdfEVx__Miscellaneous.html#gabe76647f9bd30cc72728b57cd9eb8323", null ]
];
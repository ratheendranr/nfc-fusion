var group__phalMfpEVx =
[
    [ "Component : Software", "d6/df1/group__phalMfpEVx__Sw.html", "d6/df1/group__phalMfpEVx__Sw" ],
    [ "ErrorCodes", "da/d2b/group__phalMfpEVx__Errors.html", "da/d2b/group__phalMfpEVx__Errors" ],
    [ "Common Definitions", "d3/d72/group__phalMfpEVx__CommonDefs.html", "d3/d72/group__phalMfpEVx__CommonDefs" ],
    [ "Commands_Personalization", "d4/d32/group__phalMfpEVx__Personalization.html", "d4/d32/group__phalMfpEVx__Personalization" ],
    [ "Commands_Special", "d2/d47/group__phalMfpEVx__Special.html", "d2/d47/group__phalMfpEVx__Special" ],
    [ "Utilities", "dd/d1f/group__phalMfpEVx__Utilities.html", "dd/d1f/group__phalMfpEVx__Utilities" ]
];
var structphalTop__T1T__LockCtrlTlv__t =
[
    [ "wOffset", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a089bcdf110e83f03e237725784cb8a11", null ],
    [ "wByteAddr", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a76aeb5b8e9508bd0d1bc349ea753d70a", null ],
    [ "bSizeInBits", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a0793fd80d874a1274a63334cf51b534a", null ],
    [ "bBytesPerPage", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a31e9e503fbb8b90e7dda4096aaf43c8c", null ],
    [ "bBytesLockedPerBit", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a2b84e12c70d2417b330a1f9ce7f256f3", null ]
];
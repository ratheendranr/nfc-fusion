/*
 * ndef_msg.c
 *
 *  Created on: Apr. 10, 2021
 *      Author: pankil
 */

//#TODO: - Add the description of the function names
//		 - Change the variable name if needed

#include <stdlib.h>
#include <stdio.h>
#include <Nfcrdlib_SimplifiedApi_ISO_MFUL.h>
#include "ndef_msg.h"
#include "ndef_print.h"
#include "ndef_wifi_record.h"


static ndef_msg_ctx_t g_ndef_msg_ctx;

extern phNfcLib_Transmit_t phNfcLib_TransmitInput;
extern phNfcLib_PeerInfo_t PeerInfo;

extern uint8_t bMoreDataAvailable;
extern uint16_t wNumberofBytes;
extern uint8_t  bDataBuffer[256];

static void NfcNdefResetCtx(ndef_msg_ctx_t *ctx)
{
	g_ndef_msg_ctx.msg_length = 0;
	g_ndef_msg_ctx.msg_start_idx = 0;
	g_ndef_msg_ctx.buffer_size = 0;
	g_ndef_msg_ctx.tag_capacity = 0;
	g_ndef_msg_ctx.record_count = 0;
	g_ndef_msg_ctx.first_page	= 0;
}


static uint32_t NfcReadCapabilityContainer(ndef_msg_ctx_t *ctx)
{
	uint32_t dwStatus;	
	
	/* read page 3 */
   MFUL_READ(3)

   /* Transmit will execute the command the returned data can be read by Receive command */
   dwStatus = phNfcLib_Transmit(&phNfcLib_TransmitInput,
                                0x0  /* This length paramter is used only when apart from the command, there is some data to be send*/
                                );    
    
   /* This parameter has to be reset before every receive */
   wNumberofBytes = 256;

   /* To perform receive operation to get back the read data */
  	dwStatus =  phNfcLib_Receive(&bDataBuffer[0],
                                &wNumberofBytes,
                                &bMoreDataAvailable
                                );
                                
   if((dwStatus == PH_NFCLIB_STATUS_SUCCESS) ||(wNumberofBytes == 16))
   {
    	g_ndef_msg_ctx.tag_capacity = bDataBuffer[2] * 8;
    	DEBUG_PRINTF("\n*****NTAG Message Information*****");
    	DEBUG_PRINTF("\n Tag Capacity: %d", g_ndef_msg_ctx.tag_capacity);
   }    
   else 
   {
    	DEBUG_PRINTF("\n Error reading the data from tag, Error code: 0x%8X", dwStatus);
 	}
 	return dwStatus;
}

static bool NfcFindMFULNdefMsg(ndef_msg_ctx_t *ctx)
{
	uint32_t  dwStatus ;
	
	/* start reading from page 4 */
    MFUL_READ(4)

    /* Transmit will execute the command the returned data can be read by Receive command */
    dwStatus = phNfcLib_Transmit(&phNfcLib_TransmitInput,
                                0x0  /* This length paramter is used only when apart from the command, there is some data to be send*/
                                );    
    
    /* This parameter has to be reset before every receive */
    wNumberofBytes = 256;

    /* To perform receive operation to get back the read data */
    dwStatus =  phNfcLib_Receive(&bDataBuffer[0],
                                &wNumberofBytes,
                                &bMoreDataAvailable
                                );
    
    //phApp_Print_Buff(&bDataBuffer[0], wNumberofBytes);
    /* The status should be success and the number of bytes received should be 16 for MIFARE Ultralight cards */
    if((dwStatus == PH_NFCLIB_STATUS_SUCCESS) ||(wNumberofBytes == 16))
    {
        if (bDataBuffer[0] == 0x03)
        {
            ctx->msg_length = bDataBuffer[1];
            ctx->msg_start_idx = 2;
            ctx->first_page = 4;
        }
        // page 5 byte 1
        else if (bDataBuffer[5] == 0x3) 
        {
            ctx->msg_length = bDataBuffer[6];
            ctx->msg_start_idx = 7;
            ctx->first_page = 5;
        }
        else
        {
        	return false;
        }
    }
    
   	ctx->buffer_size = ctx->msg_length + ctx->msg_start_idx + 1;
   
   	if(MFUL_READ_SIZE % MFUL_READ_SIZE != 0)
   	{
   		ctx->buffer_size = ((ctx->buffer_size/MFUL_READ_SIZE) + 1) * MFUL_READ_SIZE;
   	}
	
	DEBUG_PRINTF("\n Message length: %d", ctx->msg_length);
	DEBUG_PRINTF("\n Ndef start idx: %d", ctx->msg_start_idx);
	DEBUG_PRINTF("\n Buffer size required: %d", ctx->buffer_size);

	return true;
}


static uint32_t NfcReadMFULNdefMsg(ndef_msg_ctx_t *ctx)
{
	uint32_t  dwStatus = PH_NFCLIB_STATUS_SUCCESS;
	uint8_t idx = 0;
	
	for(int i=0; i < (ctx->msg_length/16); i++)
	{
		/* MFUL read from page 4 */
    	MFUL_READ((i*4) + ctx->first_page);
    	
    	/* Transmit will execute the command the returned data can be read by Receive command */
    	dwStatus = phNfcLib_Transmit(&phNfcLib_TransmitInput,
                                0x0  /* This length paramter is used only when apart from the command, there is some data to be send*/
                                );

    	/* The status should be success and the number of bytes received should be 16 for MIFARE Ultralight cards */
    	if(dwStatus != PH_NFCLIB_STATUS_SUCCESS)
    	{
    		DEBUG_PRINTF("\n Err: Transmit fail");
        	return dwStatus;
    	}
      
      	/* This parameter has to be reset before every receive */
    	wNumberofBytes = 256;

    	/* To perform receive operation to get back the read data */
    	dwStatus =  phNfcLib_Receive(&bDataBuffer[i*16],
                                &wNumberofBytes,
                                &bMoreDataAvailable
                                );

    	/* The status should be success and the number of bytes received should be 16 for MIFARE Ultralight cards */
    	if((dwStatus != PH_NFCLIB_STATUS_SUCCESS) ||(wNumberofBytes != 16))
    	{
    		DEBUG_PRINTF("\n Err: Received bytes are less then 16 or failed");
        	return dwStatus;
    	}
    	//DEBUG_PRINTF("\nRead Data from block %d is",(i*4) + ctx->first_page);
    	//phApp_Print_Buff(&buffer[i*16], 4);
    	//DEBUG_PRINTF("\nRead Data from block %d is",(i*4) + ctx->first_page + 1);
    	//phApp_Print_Buff(&buffer[(i*16)+4], 4);
    	//DEBUG_PRINTF("\nRead Data from block %d is",(i*4) + ctx->first_page + 2);
    	//phApp_Print_Buff(&buffer[(i*16)+8], 4);
		//DEBUG_PRINTF("\nRead Data from block %d is",(i*4) + ctx->first_page + 3);
    	//phApp_Print_Buff(&buffer[(i*16)+12], 4);
    	
		if(idx >= ctx->msg_length + ctx->msg_start_idx)
		{
			break;
		}

    	idx += MFUL_READ_SIZE;
	}
	
	//GetNdefRecord(&buffer[ndefStartIndex], messageLength);
	return dwStatus;
}


static void NfcRecordSetType(ndef_msg_ctx_t *ctx, const uint8_t *type, uint8_t record_cnt)
{
	if(ctx->ndef_msg[record_cnt].type_length)
	{
		free(ctx->ndef_msg[record_cnt].record_payload.payload_type_value);
	}
	ctx->ndef_msg[record_cnt].record_payload.payload_type_value = (uint8_t*)malloc(ctx->ndef_msg[record_cnt].type_length);
	memcpy(ctx->ndef_msg[record_cnt].record_payload.payload_type_value, (char *)type, ctx->ndef_msg[record_cnt].type_length);
	ctx->ndef_msg[record_cnt].record_payload.payload_type_value[ctx->ndef_msg[record_cnt].type_length] = '\0';
	DEBUG_PRINTF("\n Record type : %s", ctx->ndef_msg[record_cnt].record_payload.payload_type_value);
	//PrintHexChar(&ctx->ndef_msg[record_cnt].record_payload.payload_type_value[0], ctx->ndef_msg[record_cnt].type_length);
}


static void NfcRecordSetId(ndef_msg_ctx_t *ctx, const uint8_t *id, uint8_t record_cnt)
{
	if(ctx->ndef_msg[record_cnt].id_length)
	{
		free(ctx->ndef_msg[record_cnt].record_payload.payload_id_value);
	}
	ctx->ndef_msg[record_cnt].record_payload.payload_id_value = (uint8_t*)malloc(ctx->ndef_msg[record_cnt].id_length);
	memcpy(ctx->ndef_msg[record_cnt].record_payload.payload_id_value, (char *)id, ctx->ndef_msg[record_cnt].id_length);
	ctx->ndef_msg[record_cnt].record_payload.payload_id_value[ctx->ndef_msg[record_cnt].id_length] = '\0';	
	DEBUG_PRINTF("\n Record ID : %s",ctx->ndef_msg[record_cnt].record_payload.payload_id_value);
	//PrintHexChar(&ctx->ndef_msg[record_cnt].record_payload.payload_id_value[0], ctx->ndef_msg[record_cnt].id_length);
}

static void NfcRecordSetPayload(ndef_msg_ctx_t *ctx, const uint8_t *payload, uint8_t record_cnt)
{
	if(ctx->ndef_msg[record_cnt].payload_length)
	{
		free(ctx->ndef_msg[record_cnt].record_payload.record_value);
	}
	ctx->ndef_msg[record_cnt].record_payload.record_value = (uint8_t*)malloc(ctx->ndef_msg[record_cnt].payload_length);
	memcpy(ctx->ndef_msg[record_cnt].record_payload.record_value, (char *)payload, ctx->ndef_msg[record_cnt].payload_length);
	ctx->ndef_msg[record_cnt].record_payload.record_value[ctx->ndef_msg[record_cnt].payload_length] = '\0';
	DEBUG_PRINTF("\n Record Payload : %s", ctx->ndef_msg[record_cnt].record_payload.record_value);
	//PrintHexChar(&ctx->ndef_msg[record_cnt].record_payload.record_value[0], ctx->ndef_msg[record_cnt].payload_length);
}



static void NfcProcessMFULNdefMsg(ndef_msg_ctx_t *ctx)
{
	uint8_t tnf_byte;
	int idx = ctx->msg_start_idx;
	uint8_t record_count = 0;

	while(idx < ctx->msg_length)
	{
		if(record_count < MAX_NDEF_RECORD)
		{
			tnf_byte = bDataBuffer[idx];
			ctx->ndef_msg[record_count].first_byte.MB = tnf_byte & 0x80;
			ctx->ndef_msg[record_count].first_byte.TNF = tnf_byte & 0x07;
			ctx->ndef_msg[record_count].first_byte.ME = tnf_byte & 0x40;
			ctx->ndef_msg[record_count].first_byte.SR = tnf_byte & 0x10;
			ctx->ndef_msg[record_count].first_byte.IL = tnf_byte & 0x08;
			ctx->ndef_msg[record_count].first_byte.CF = tnf_byte & 0x20;
			
			DEBUG_PRINTF("\n\n\n .......Record number %d details.......", record_count+1);
			DEBUG_PRINTF("\n MB : %d", ctx->ndef_msg[record_count].first_byte.MB & 0x80 ? 1 : 0);
			DEBUG_PRINTF("\n ME : %d", ctx->ndef_msg[record_count].first_byte.ME & 0x40 ? 1 : 0);
			DEBUG_PRINTF("\n CF : %d", ctx->ndef_msg[record_count].first_byte.CF & 0x20 ? 1 : 0);
			DEBUG_PRINTF("\n SR : %d", ctx->ndef_msg[record_count].first_byte.SR & 0x10 ? 1 : 0);
			DEBUG_PRINTF("\n IL : %d", ctx->ndef_msg[record_count].first_byte.IL & 0x08 ? 1 : 0);
			DEBUG_PRINTF("\n TNF : %d", ctx->ndef_msg[record_count].first_byte.TNF);
			
			idx++;

			ctx->ndef_msg[record_count].type_length = bDataBuffer[idx];
			
			if(ctx->ndef_msg[record_count].first_byte.SR)
			{
				idx++;
				ctx->ndef_msg[record_count].payload_length = bDataBuffer[idx];
			}
			else 
			{
				ctx->ndef_msg[record_count].payload_length = ((uint32_t) bDataBuffer[idx] << 24)		|
												((uint32_t) bDataBuffer[idx+1] << 16) 	|
												((uint32_t) bDataBuffer[idx+2] << 8) 	|
												((uint32_t) bDataBuffer[idx]);
				idx += 4;
			}		
			
			if(ctx->ndef_msg[record_count].first_byte.IL)
			{
				idx++;
				ctx->ndef_msg[record_count].id_length = bDataBuffer[idx];
			}
			
			idx++;

			ctx->ndef_msg[record_count].payload_type = bDataBuffer[idx];
			
			NfcRecordSetType(ctx , &bDataBuffer[idx], record_count);
			
			idx += ctx->ndef_msg[record_count].type_length;
			
			if(ctx->ndef_msg[record_count].first_byte.IL)
			{
				NfcRecordSetId(ctx, &bDataBuffer[idx], record_count);
				idx += ctx->ndef_msg[record_count].id_length;
			}
			
			NfcRecordSetPayload(ctx, &bDataBuffer[idx], record_count);
			
			idx += ctx->ndef_msg[record_count].payload_length;

			ctx->record_count++;
			
			record_count++;
			//DEBUG_PRINTF("\n idx: %d", idx);
		}
	}
}


static void NfcProcessMMIERecord(ndef_record_t *mime_record)
{
	const char *wsc_record_type = "application/vnd.wfa.wsc";
	const char *special_poster = "Sp";
	if(strcmp(mime_record->record_payload.payload_type_value, wsc_record_type) == 0)
	{
		//We get the wifi simple configuration record from the tag
		//Now decoding the record
		NfcProcessWifiRecord(mime_record);
	}
	else if(strcmp(mime_record->record_payload.payload_type_value, special_poster) == 0)
	{
		DEBUG_PRINTF("Special poster MIME record, not supporting decoding for now");	
	}
	
}


static void NfcProcessRecords(ndef_msg_ctx_t *ctx)
{
	DEBUG_PRINTF("\n Decoding Records..... ");
	for(int i=0; i < ctx->record_count; i++)
	{
		
		DEBUG_PRINTF("\n\n\n ....Decoding Record No: %d.....", i+1);

		switch(ctx->ndef_msg[i].first_byte.TNF)
		{
			case 0X00:
				DEBUG_PRINTF("\n empty");
			break;

			case 0x01:
				DEBUG_PRINTF("\n Well Known, Defined in NFC Forum RTD Spec.");
			break;

			case 0x02:
				DEBUG_PRINTF("\n MIME Media-type, defined in RFC 2046");
				NfcProcessMMIERecord(&ctx->ndef_msg[i]);
				//if(ctx->ndef_msg[i].)
				//NfcProcessWifiRecord(&ctx->ndef_msg[i]);
			break;

			case 0x03:
				DEBUG_PRINTF("\n Absolute URI, defined in RFC 3986");
			break;

			case 0x04:
				DEBUG_PRINTF("\n External, User defined");
			break;

			case 0x05:
				DEBUG_PRINTF("\n Unknown");
			break;

			case 0X06:
				DEBUG_PRINTF("\n Unchanged");
			break;

			case 0x07:
				DEBUG_PRINTF("\n Reserved");
			break;
			
			default:
			break;
		}
	}
}

uint32_t NfcFindNdefMsg(uint8_t tag_type)
{
   int page;
   uint32_t  dwStatus;
    
   NfcNdefResetCtx(&g_ndef_msg_ctx);
   memset(&bDataBuffer[0], 0x00, sizeof(bDataBuffer));
   
	dwStatus = NfcReadCapabilityContainer(&g_ndef_msg_ctx);  
   
   if(dwStatus != PH_NFCLIB_STATUS_SUCCESS)
   {
   		return dwStatus;
   }

   //Tag type 1 represents Mifare ultralight or NTag
   if(tag_type == 1)
   {
   		if(NfcFindMFULNdefMsg(&g_ndef_msg_ctx))
   		{
   			if(g_ndef_msg_ctx.msg_length != 0)
   			{
   				dwStatus = NfcReadMFULNdefMsg(&g_ndef_msg_ctx);
   				//If ndef messgae is available then read NDEF data from the tag
   				if( dwStatus != PH_NFCLIB_STATUS_SUCCESS)
   				{
   					return dwStatus;
   				}

   				//Seperate the records from the NDEF messgae and store them in seperate buffer 
   				NfcProcessMFULNdefMsg(&g_ndef_msg_ctx);

   				//Decode the NDEF record from the message
   				NfcProcessRecords(&g_ndef_msg_ctx);
   			}
   		}
   }
   return dwStatus;
}

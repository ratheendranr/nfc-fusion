var group__phalVca__Utilities =
[
    [ "PHAL_VCA_ADDITIONAL_INFO", "d8/dc0/group__phalVca__Utilities.html#gac58ab9fb293d25fbcca30b39316f7f7e", null ],
    [ "PHAL_VCA_WRAPPED_MODE", "d8/dc0/group__phalVca__Utilities.html#ga3f31cf24407356c54becb327cc06ee38", null ],
    [ "PHAL_VCA_TIMING_MODE", "d8/dc0/group__phalVca__Utilities.html#ga84d54e615e89c602a0c2325f11719497", null ],
    [ "PHAL_VCA_PC_LOWER_THRESHOLD", "d8/dc0/group__phalVca__Utilities.html#ga0c465c722a9f1f357daf929839fe7e1e", null ],
    [ "PHAL_VCA_PC_EXTENDED_APDU", "d8/dc0/group__phalVca__Utilities.html#gad113c8a6b7055197e505a9e839b91bbe", null ],
    [ "phalVca_SetConfig", "d8/dc0/group__phalVca__Utilities.html#gaaeed3ce58723983334aa66be71341e3d", null ],
    [ "phalVca_GetConfig", "d8/dc0/group__phalVca__Utilities.html#ga54eaeda65d0558d732802c1eaa568aed", null ],
    [ "phalVca_SetSessionKeyUtility", "d8/dc0/group__phalVca__Utilities.html#gaa6aa12089df9b16312a1986333669db8", null ],
    [ "phalVca_SetApplicationType", "d8/dc0/group__phalVca__Utilities.html#ga7d5e2a5f9ac28344d655f925754959bb", null ],
    [ "phalVca_DecryptResponse", "d8/dc0/group__phalVca__Utilities.html#ga3b862fa69b892eb852ce05c8cc860d5f", null ]
];
var structphalTop__T2T__LockCtrlTlv__t =
[
    [ "wOffset", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#ad8bc59b76a5a54f03070b33dae40d032", null ],
    [ "wByteAddr", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#ae4579c81e8e24cf2d55b68b2c766fd5b", null ],
    [ "bSizeInBits", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#aa36af0441952d4fa471a848bbbf47e77", null ],
    [ "bBytesPerPage", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#a795e9766456edf504e6f7999977ead85", null ],
    [ "bBytesLockedPerBit", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#af077c6cc686b7cc7a91190e98bc0d8c7", null ]
];
var group__phalMfpEVx__Utilities =
[
    [ "PHAL_MFPEVX_WRAPPED_MODE", "dd/d1f/group__phalMfpEVx__Utilities.html#ga8436d1ab49e6d7565c6b8912ecf2f654", null ],
    [ "PHAL_MFPEVX_EXTENDED_APDU", "dd/d1f/group__phalMfpEVx__Utilities.html#gab4b8305ac96e3233e0f1ff527d42fe0f", null ],
    [ "PHAL_MFPEVX_AUTH_MODE", "dd/d1f/group__phalMfpEVx__Utilities.html#gafc4fcc0f10e90963d6e2bcc657e07284", null ],
    [ "PHAL_MFPEVX_DISABLE", "dd/d1f/group__phalMfpEVx__Utilities.html#ga81a38e9417889ceb507f7bdd12c37791", null ],
    [ "PHAL_MFPEVX_ENABLE", "dd/d1f/group__phalMfpEVx__Utilities.html#ga4b288f4deb3c80151aea9cd2279baef2", null ],
    [ "PHAL_MFPEVX_DEFAULT", "dd/d1f/group__phalMfpEVx__Utilities.html#gaf2690cb0e642c5ab47908fa074bde9ba", null ],
    [ "PHAL_MFPEVX_NOTAUTHENTICATED", "dd/d1f/group__phalMfpEVx__Utilities.html#ga5d880a370b638ff99ea7a03dbe2c4e57", null ],
    [ "PHAL_MFPEVX_SL1_MIFARE_AUTHENTICATED", "dd/d1f/group__phalMfpEVx__Utilities.html#ga0fc4e0709355bebf3eda7521c4e87c99", null ],
    [ "PHAL_MFPEVX_SL1_MFP_AUTHENTICATED", "dd/d1f/group__phalMfpEVx__Utilities.html#gae8bb485e804624df761bf3cfef111448", null ],
    [ "PHAL_MFPEVX_SL3_MFP_AUTHENTICATED", "dd/d1f/group__phalMfpEVx__Utilities.html#gab90fc52ab77e4b4bd88c96a6fb5db428", null ],
    [ "PHAL_MFPEVX_NOT_AUTHENTICATED_L3", "dd/d1f/group__phalMfpEVx__Utilities.html#gafa4de015a7ec7a84aab629f4700c5e0d", null ],
    [ "PHAL_MFPEVX_NOT_AUTHENTICATED_L4", "dd/d1f/group__phalMfpEVx__Utilities.html#ga57f31f265cfb2711cdbc61b1e59f9783", null ],
    [ "phalMfpEVx_ResetSecMsgState", "dd/d1f/group__phalMfpEVx__Utilities.html#gab4d57abf11243e59743a8be33ad16901", null ],
    [ "phalMfpEVx_SetConfig", "dd/d1f/group__phalMfpEVx__Utilities.html#ga3e20ba3a902fe98f638f4b82bb89a222", null ],
    [ "phalMfpEVx_GetConfig", "dd/d1f/group__phalMfpEVx__Utilities.html#ga1adce32d8564174695c153787c805731", null ],
    [ "phalMfpEVx_SetVCAParams", "dd/d1f/group__phalMfpEVx__Utilities.html#ga4bdff3058b2314b7d934ffc0a99d2976", null ]
];
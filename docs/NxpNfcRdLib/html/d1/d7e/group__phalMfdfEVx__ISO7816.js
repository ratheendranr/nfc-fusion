var group__phalMfdfEVx__ISO7816 =
[
    [ "PHAL_MFDFEVX_FCI_RETURNED", "d1/d7e/group__phalMfdfEVx__ISO7816.html#gaa967d9522452fb0ada66eea76246fb76", null ],
    [ "PHAL_MFDFEVX_FCI_NOT_RETURNED", "d1/d7e/group__phalMfdfEVx__ISO7816.html#gaf0e18723f3f9e14f2609adac3d69a991", null ],
    [ "PHAL_MFDFEVX_SELECTOR_0", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga6ec44d3facfb697099dd932ecdbe422e", null ],
    [ "PHAL_MFDFEVX_SELECTOR_1", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga424c51dda8fdceba72fc6ce7b4e7696f", null ],
    [ "PHAL_MFDFEVX_SELECTOR_2", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga41d9755cabc5b91c50cac4e418443716", null ],
    [ "PHAL_MFDFEVX_SELECTOR_3", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga02de1b980712b4696e74a1dc5b8304da", null ],
    [ "PHAL_MFDFEVX_SELECTOR_4", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga8e1995be5be4bcec83800e0c809964c4", null ],
    [ "phalMfdfEVx_IsoSelectFile", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga49098c1c76682bb7d6450a8ebb9665d3", null ],
    [ "phalMfdfEVx_IsoReadBinary", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga8931af8378962be16e3191c30b74b9c4", null ],
    [ "phalMfdfEVx_IsoUpdateBinary", "d1/d7e/group__phalMfdfEVx__ISO7816.html#gac189da6b0de9b308ef2d1a545e345270", null ],
    [ "phalMfdfEVx_IsoReadRecords", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga235d336321830088998ffdcd349693cb", null ],
    [ "phalMfdfEVx_IsoAppendRecord", "d1/d7e/group__phalMfdfEVx__ISO7816.html#gabf02f5799e23ed9905b056bd37ad0bac", null ],
    [ "phalMfdfEVx_IsoUpdateRecord", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga42c95512eae9d93232044606440d3abf", null ],
    [ "phalMfdfEVx_IsoGetChallenge", "d1/d7e/group__phalMfdfEVx__ISO7816.html#ga15440b6af0fed7f2bfa4660d7a09403f", null ]
];
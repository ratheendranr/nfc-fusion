var NAVTREE =
[
  [ "NXP NFC Reader Library", "index.html", [
    [ "Introduction", "index.html#phsec_Intro", null ],
    [ "Overview", "index.html#phsec_Overview", null ],
    [ "LayerModel", "index.html#phsec_LayerModel", null ],
    [ "API Reference", "index.html#phsec_ApiReference", [
      [ "Simplified API Layer", "index.html#phsec_RefSA", null ],
      [ "Network Protocol Layer", "index.html#phsec_RefNp", null ],
      [ "NFC Link Layer", "index.html#phsec_RefLn", null ],
      [ "Card Emulation Layer", "index.html#phsec_RefCe", null ],
      [ "Activity Layer", "index.html#phsec_RefAc", null ],
      [ "Application Layer", "index.html#phsec_RefAl", null ],
      [ "Protocol Abstraction Layer", "index.html#phsec_RefPal", null ],
      [ "Hardware Abstraction Layer", "index.html#phsec_RefHal", null ],
      [ "Common Layer", "index.html#phsec_RefCommon", null ],
      [ "Other", "index.html#phsec_RefOther", null ],
      [ "References", "index.html#phsec_RefReferences", null ]
    ] ],
    [ "DISCLAIMER OF WARRANTIES:", "index.html#phMfpLib_DisclaimerInfo", null ],
    [ "LIMITATION OF LIABILITY:", "index.html#phMfpLib_LiabilityInfo", null ],
    [ "Revision History", "index.html#phsec_RevisionHistory", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"d0/d4f/group__phalVca__PC.html#ga558e8cec547dfb68bb1f08b38cca1edf",
"d1/d45/structphpalI14443p4__Sw__DataParams__t.html#ac30f65ccf77e2665ee6cb672dfb36c7a",
"d2/d55/structphalTop__MfcTop__t.html#aa22f3f5064c71634b8031654c4106fa1",
"d3/dbf/group__phhalHw__Pn5190.html#gaf3e26de2ac1639cb5af2e65511086103",
"d5/d3d/structphalTop__T2T__Sector__t.html",
"d6/d19/group__phhalHw__Pn5180.html#ga3498bf8bec358d8bf00571a13fe8be04",
"d6/d78/group__phhalHw.html#ga8c1646388e667e3f74bd9c819b3f9fdc",
"d7/da7/structphalMfNtag42XDna__Sw__DataParams__t.html#ac51ef992d70aa698c6827378482bdbb3",
"d8/d39/structphhalHw__Pn5180__DataParams__t.html#a26964e02cd751b58ad4c9631dfa8bec8",
"d8/d4c/group__ph__NxpBuild.html#ga8edc805a11611fef9f3b6db1318454b6",
"d9/d2b/group__ph__RefDefs.html",
"da/d27/group__phalMfdfEVx__Sw.html",
"db/d56/group__phTools.html#ga95a9ded1d2cb8860c7fdcf724071c157",
"dc/d52/group__ph__Private.html#ga3e7685127c05c179717ce6589ae7f58f",
"dc/d52/group__ph__Private.html#gab0e4b8c4e3c912f579935a20853305ac",
"dc/dce/group__ph__Error.html#ga46abb608bf2b65530ff03dbe99b88784",
"dd/d44/group__phhalHw__Rc663__Int.html#gaa086693d7d4a9dc44ff00ac280f34251",
"de/dad/group__phhalHw__PN5190__Instr.html#ga2da004ba012c8bfea818242ad0daafb8",
"df/d5c/group__phalMful.html#ga711b9b597891629ca9f16f5ddfb17efc"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
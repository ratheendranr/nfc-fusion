/*
 * ndef_print.h
 *
 *  Created on: Apr. 10, 2021
 *      Author: pankil
 */

#ifndef NDEF_PRINT_H_
#define NDEF_PRINT_H_

void PrintHex(const uint8_t *data, const long numBytes);
void PrintHexChar(const uint8_t *data, const long numBytes);
void DumpHex(const uint8_t *data, const long numBytes, const unsigned int blockSize);

#endif//NDEF_PRINT_H_

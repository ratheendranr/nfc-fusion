var group__phalMfpEVx__PICC__Errors =
[
    [ "PHAL_MFPEVX_RESP_NACK0", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga72e943bd1b83aaf5bc54625020630179", null ],
    [ "PHAL_MFPEVX_RESP_NACK1", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga1eca45d7f4047e37a5bd059b6d2bf519", null ],
    [ "PHAL_MFPEVX_RESP_NACK4", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga21e51b8b90256bc1573266f505322961", null ],
    [ "PHAL_MFPEVX_RESP_NACK5", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga52576d57ab0a15ca32a2d6a1f9999bc8", null ],
    [ "PHAL_MFPEVX_RESP_ACK_ISO3", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#gad3d0b4fe031c040dc7c30e5fa1677cd8", null ],
    [ "PHAL_MFPEVX_RESP_ACK_ISO4", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga9f0a7ccefe863a54ab685ef74d332c68", null ],
    [ "PHAL_MFPEVX_RESP_ERR_TM", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#gaccd42edcdc2a9c72feb7d4100abd8507", null ],
    [ "PHAL_MFPEVX_RESP_ERR_AUTH", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga67729282c857ea7251673b4ec994ed7a", null ],
    [ "PHAL_MFPEVX_RESP_ERR_CMD_OVERFLOW", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga2c73026f18571fc4558bd5181af943de", null ],
    [ "PHAL_MFPEVX_RESP_ERR_MAC_PCD", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga2f639626507e9689bab8c2f62e0efa52", null ],
    [ "PHAL_MFPEVX_RESP_ERR_BNR", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#gab142d08caef7afc0e064e67c7f721d24", null ],
    [ "PHAL_MFPEVX_RESP_ERR_EXT", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga6b07b3e66861f075a66b9065fc79091c", null ],
    [ "PHAL_MFPEVX_RESP_ERR_CMD_INVALID", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#gae0d2933128222a0da6db188be6656be4", null ],
    [ "PHAL_MFPEVX_RESP_ERR_FORMAT", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga1bf7585058d269eab0d02b148282fce8", null ],
    [ "PHAL_MFPEVX_RESP_ERR_NOT_SUP", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga7d066b4c692db523573f278addf1fd31", null ],
    [ "PHAL_MFPEVX_RESP_ERR_GEN_FAILURE", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga0d31c4197ae415ab30152866d7022a74", null ],
    [ "PHAL_MFPEVX_RESP_ADDITIONAL_FRAME", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#gabaef30d2f7fa2af434a4d39bbc278365", null ],
    [ "PHAL_MFPEVX_ISO7816_RESP_SUCCESS", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#gad6c65ce730f4062f422fa779bc6c8d5b", null ],
    [ "PHAL_MFPEVX_ISO7816_RESP_ERR_WRONG_LENGTH", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga66ce5e3cbd66c339ea518efe9c1d591f", null ],
    [ "PHAL_MFPEVX_ISO7816_RESP_ERR_WRONG_PARAMS", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga0101e4aa3e79e5e8371af298d627d5e9", null ],
    [ "PHAL_MFPEVX_ISO7816_RESP_ERR_WRONG_LC", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga8d0d5e8cfaa248e424d7e9aac83dfc9d", null ],
    [ "PHAL_MFPEVX_ISO7816_RESP_ERR_WRONG_LE", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga2eff5e5d37495a42bc5eba8ce358c0ce", null ],
    [ "PHAL_MFPEVX_ISO7816_RESP_ERR_WRONG_CLA", "d4/d5d/group__phalMfpEVx__PICC__Errors.html#ga7f180bcb7a032332bdf685c9ca869f69", null ]
];
var group__phalMfdfEVx__DataManagement =
[
    [ "PHAL_MFDFEVX_APPLICATION_CHAINING", "dc/d69/group__phalMfdfEVx__DataManagement.html#ga36d43af40fb5c16e82ebcfb7a9c80e28", null ],
    [ "PHAL_MFDFEVX_ISO_CHAINING", "dc/d69/group__phalMfdfEVx__DataManagement.html#gac6d58b642bdf91d7d529af0cd587e75f", null ],
    [ "phalMfdfEVx_ReadData", "dc/d69/group__phalMfdfEVx__DataManagement.html#ga3995f0730328e11b135c646dd4a20034", null ],
    [ "phalMfdfEVx_WriteData", "dc/d69/group__phalMfdfEVx__DataManagement.html#gaa31c7d2dc5b395007ba048e6311687cb", null ],
    [ "phalMfdfEVx_GetValue", "dc/d69/group__phalMfdfEVx__DataManagement.html#ga64480abb1a1f45ae283d733c7bb7e98b", null ],
    [ "phalMfdfEVx_Credit", "dc/d69/group__phalMfdfEVx__DataManagement.html#ga15e32fe223d8bfe697fc762c18b040d4", null ],
    [ "phalMfdfEVx_Debit", "dc/d69/group__phalMfdfEVx__DataManagement.html#ga0ed2a13501a3d62751f05d1867012b2f", null ],
    [ "phalMfdfEVx_LimitedCredit", "dc/d69/group__phalMfdfEVx__DataManagement.html#ga8b73185b7251d3dae4d1802a6219245c", null ],
    [ "phalMfdfEVx_ReadRecords", "dc/d69/group__phalMfdfEVx__DataManagement.html#ga0b50c02597cdaf1cfbb6a092bb608850", null ],
    [ "phalMfdfEVx_WriteRecord", "dc/d69/group__phalMfdfEVx__DataManagement.html#ga2c351f0a0107f39cb8452d5d17284d7d", null ],
    [ "phalMfdfEVx_UpdateRecord", "dc/d69/group__phalMfdfEVx__DataManagement.html#gaf411969fbefdd99997aba04e1e90461b", null ],
    [ "phalMfdfEVx_ClearRecordFile", "dc/d69/group__phalMfdfEVx__DataManagement.html#ga0a8b2a06bfba88414f6b0b6ed19a71dd", null ]
];
var structphalVca__Sw__DataParams__t =
[
    [ "wId", "df/d56/structphalVca__Sw__DataParams__t.html#aa0d6558483263bfc30964670290da61d", null ],
    [ "pPalMifareDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#a0e3d2e2ac96ab198c9b4ee91cdac5070", null ],
    [ "pKeyStoreDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#ad5b4d4b082953e68bf23d407f20d3102", null ],
    [ "pCryptoDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#a763cad67213cde29e0c929dba74772bc", null ],
    [ "pCryptoRngDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#a89568d21e4e117533b9874b6ba1db85d", null ],
    [ "pCardTable", "df/d56/structphalVca__Sw__DataParams__t.html#aaa2f41ca8e1512495ec5a2c3cc084303", null ],
    [ "pIidTable", "df/d56/structphalVca__Sw__DataParams__t.html#acdb19cd829102c52bf98d02ca49e2e55", null ],
    [ "wCurrentCardTablePos", "df/d56/structphalVca__Sw__DataParams__t.html#a07cf77633b3fa2176694343c8a54373b", null ],
    [ "wNumCardTableEntries", "df/d56/structphalVca__Sw__DataParams__t.html#a1d0145c775ccc682988230704ac38217", null ],
    [ "wNumIidTableEntries", "df/d56/structphalVca__Sw__DataParams__t.html#a5e389f853d358b7a2ff245fb1ca71d89", null ],
    [ "wCurrentIidIndex", "df/d56/structphalVca__Sw__DataParams__t.html#aebecabe16d3b95efa56c0da52b776bb8", null ],
    [ "wCurrentIidTablePos", "df/d56/structphalVca__Sw__DataParams__t.html#afb54dc5ebd4733e262be2a63f41969cc", null ],
    [ "wAdditionalInfo", "df/d56/structphalVca__Sw__DataParams__t.html#a67e9113c9d0fca83255ea15526119324", null ],
    [ "bSessionAuthMACKey", "df/d56/structphalVca__Sw__DataParams__t.html#af80c51c2ec7b627a8d7b5b389a1708ff", null ],
    [ "eVCState", "df/d56/structphalVca__Sw__DataParams__t.html#a2d91c2b33845c41e8b6e5aec2887ba77", null ],
    [ "ePCState", "df/d56/structphalVca__Sw__DataParams__t.html#ae4bb8898a4fd0b767c65d08cb5f1b7b9", null ],
    [ "pAlDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#a487770466acb618bb75efb835880210c", null ],
    [ "bWrappedMode", "df/d56/structphalVca__Sw__DataParams__t.html#a926afe433187903a59cadef7ed625fb1", null ],
    [ "bExtendedLenApdu", "df/d56/structphalVca__Sw__DataParams__t.html#ae6d537197aeffdcc0f307f272d2c05e5", null ],
    [ "bOption", "df/d56/structphalVca__Sw__DataParams__t.html#a721bcbd8fda5fcbfb4ca3d2492c6db45", null ],
    [ "bLowerBoundThreshold", "df/d56/structphalVca__Sw__DataParams__t.html#aac24673dafb92ae329739919b64b9a08", null ]
];
var group__phalVca__VC =
[
    [ "PHAL_VCA_ISO_SELECT_SELECTION_DF_NAME", "dc/da7/group__phalVca__VC.html#ga90b81489b43f2f390fd22cb7428a5b56", null ],
    [ "PHAL_VCA_ISO_SELECT_FCI_RETURNED", "dc/da7/group__phalVca__VC.html#ga1f659e3de296d0a0bcaf44d8ad9cd1a3", null ],
    [ "PHAL_VCA_ISO_SELECT_VARIANT_PART1", "dc/da7/group__phalVca__VC.html#ga2b01cee4b2a56873e57a7873ddf4144d", null ],
    [ "PHAL_VCA_ISO_SELECT_VARIANT_PART2", "dc/da7/group__phalVca__VC.html#ga71d7db5933e616be211cfcc64f109f37", null ],
    [ "PHAL_VCA_ISO_SELECT_DIV_DISABLED", "dc/da7/group__phalVca__VC.html#ga219b5fb8f2dd1cd569a67bc3710e60fb", null ],
    [ "PHAL_VCA_ISO_SELECT_ENC_KEY_DIV_INPUT", "dc/da7/group__phalVca__VC.html#ga111282d46651de5bbc0ba29074d3ef1d", null ],
    [ "PHAL_VCA_ISO_SELECT_MAC_KEY_DIV_INPUT", "dc/da7/group__phalVca__VC.html#ga24175b2838574cad750960ad7a756858", null ],
    [ "PHAL_VCA_ISO_SELECT_MAC_KEY_DIV_VCUID", "dc/da7/group__phalVca__VC.html#ga548dc8e0fbce33806b1ec58bde1fa4d8", null ],
    [ "PHAL_VCA_ISO_SELECT_MAC_KEY_DIV_INPUT_VCUID", "dc/da7/group__phalVca__VC.html#gacaed22c89d52782714d610148c0ca593", null ],
    [ "phalVca_StartCardSelection", "dc/da7/group__phalVca__VC.html#ga130495aadf9a346108454a3db7fef49c", null ],
    [ "phalVca_FinalizeCardSelection", "dc/da7/group__phalVca__VC.html#gaa857909bd2484b1836a0c9e36afe139b", null ],
    [ "phalVca_SelectVc", "dc/da7/group__phalVca__VC.html#gadf92879dc31d05cd857747401e7520b4", null ],
    [ "phalVca_DeselectVc", "dc/da7/group__phalVca__VC.html#ga591e72ed1de2ad1eb29271d53ca242fe", null ],
    [ "phalVca_VcSupport", "dc/da7/group__phalVca__VC.html#gab386b1a92b0679cd7f4b1408500e3bd9", null ],
    [ "phalVca_VcSupportLast", "dc/da7/group__phalVca__VC.html#ga76c20997016cadc902ab014aca541b3b", null ],
    [ "phalVca_GetIidInfo", "dc/da7/group__phalVca__VC.html#ga0bc004567315a356b928ec988558bd28", null ],
    [ "phalVca_IsoSelect", "dc/da7/group__phalVca__VC.html#ga1a897414a214dd1226f16d66b5d82b47", null ],
    [ "phalVca_IsoExternalAuthenticate", "dc/da7/group__phalVca__VC.html#ga8281db0926237692daae6fb9f7ba0b5b", null ]
];
var group__phhalHw__Pn5190 =
[
    [ "Instruction", "de/dad/group__phhalHw__PN5190__Instr.html", "de/dad/group__phhalHw__PN5190__Instr" ],
    [ "phhalHw_Pn5190_DataParams_t", "df/d78/structphhalHw__Pn5190__DataParams__t.html", [
      [ "wId", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a92118668b2b9121c794bf52b6f51abe7", null ],
      [ "bBalConnectionType", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a4ed60245cbe627c28b4b9b7e47dc8699", null ],
      [ "bRfca", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a1f466dc2636639527b209c692b91003f", null ],
      [ "pBalDataParams", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a759d417fc550f0be09ee6d73f28bc61d", null ],
      [ "pKeyStoreDataParams", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a6ac84e996758b56e2e4c7f9c39c79a87", null ],
      [ "pTxBuffer", "df/d78/structphhalHw__Pn5190__DataParams__t.html#ad5f5846073bf860f653b8548829ff51a", null ],
      [ "wTxBufSize", "df/d78/structphhalHw__Pn5190__DataParams__t.html#aaa2d5a1c41589553c5ba8ad2ff839393", null ],
      [ "wTxBufLen", "df/d78/structphhalHw__Pn5190__DataParams__t.html#ac11578645477caa2e9224c3d1c25c7cd", null ],
      [ "pRxBuffer", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a31cf7548e86a1c920850c0473b04a4dc", null ],
      [ "wRxBufSize", "df/d78/structphhalHw__Pn5190__DataParams__t.html#abbbcb34d45d8cd23b05d3f990b8ad8e1", null ],
      [ "wRxBufLen", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a11b05f350a17272d34b560f68c9b3ba3", null ],
      [ "wTxBufStartPos", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a86ee5363b9eb5f001b840147c0985e68", null ],
      [ "wRxBufStartPos", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a78f5b86071d9ece8c5cffc2045ee89d2", null ],
      [ "dwTxWaitMs", "df/d78/structphhalHw__Pn5190__DataParams__t.html#adb4060176a00c07ee01fac332cdc5f9b", null ],
      [ "dwFdtPc", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a629450f60e433dd972e2a768080e0318", null ],
      [ "wFieldOffTime", "df/d78/structphhalHw__Pn5190__DataParams__t.html#ac8d0f54e50fb3c5b8cf6dba0031a497b", null ],
      [ "wFieldRecoveryTime", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a97f5ab3b2ac6a793c1b26b8b495b49ff", null ],
      [ "wTargetMode", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a2abca6ae56882406c9d781bca93e2bdc", null ],
      [ "wWakeupCounterInMs", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a6722557492815b6c0092dc2583635a77", null ],
      [ "wLPCDWakeupCounterInMs", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a1436c85b1a6d812ef72f525ed2291cd8", null ],
      [ "bPollGuardTimeFlag", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a30f2db25e43d0ccbedbd62c51d5f5f60", null ],
      [ "bLpcdConfig", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a0e7bbf6794f2a9bd76a600740b73cdc5", null ],
      [ "bLpcdMode", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a5986c6fb9314be7c14cb38f8bd9acab3", null ],
      [ "bLpcdWakeUpCtrl", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a29a86ab9c2610d064f6f759a884906e2", null ],
      [ "bSymbolStart", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a8769ec72d2331383e3b2db537050e424", null ],
      [ "bSymbolEnd", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a941679b336bdec83161eb7b793810961", null ],
      [ "bRfResetAfterTo", "df/d78/structphhalHw__Pn5190__DataParams__t.html#af37ab030a62215610fb975bc6e39868f", null ],
      [ "bActiveMode", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a2d1b9b84fa1142b3c97b43d0641fe2f4", null ],
      [ "bJewelActivated", "df/d78/structphhalHw__Pn5190__DataParams__t.html#acf4d4b8947cc38406bee4a92da1d0e60", null ],
      [ "bCardType", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a89e0364c0ea41557cbb9feb45761dd60", null ],
      [ "bTimeoutUnit", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a529764e23d8e8648e1aefd43b73e766d", null ],
      [ "bRxMultiple", "df/d78/structphhalHw__Pn5190__DataParams__t.html#ab10de43a1ba3e65f9f6aa477ad4ba30b", null ],
      [ "bMfcCryptoEnabled", "df/d78/structphhalHw__Pn5190__DataParams__t.html#afd7e245c8ee2528ca09d8b027a93c350", null ],
      [ "bCardMode", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a0b1f331d65b3dceff4f78f443fc1ca31", null ],
      [ "bCmdAborted", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a3f2ad73c7c72767fc91971b955b227c9", null ],
      [ "pRFISRCallback", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a781cacf9d0fde986fe85f7a10691c42e", null ],
      [ "pTimerISRCallBack", "df/d78/structphhalHw__Pn5190__DataParams__t.html#af864bf3b909e997ec6b14ac629ced781", null ],
      [ "bOpeMode", "df/d78/structphhalHw__Pn5190__DataParams__t.html#ac9240dc80bc62fdee33a8530083f6472", null ],
      [ "bEmdFlag", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a7b799d39fa0031f612156fc7d2562b84", null ],
      [ "dwFelicaEmdReg", "df/d78/structphhalHw__Pn5190__DataParams__t.html#ae7f9ce7ac26a09c97208350d5a036ae9", null ],
      [ "wCfgShadow", "df/d78/structphhalHw__Pn5190__DataParams__t.html#aa06d0f68407d7cd67e8081a4d35930f5", null ],
      [ "HwEventObj", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a9b5cdee7f816db44f5949c6004ca84a5", null ],
      [ "sIrqResp", "df/d78/structphhalHw__Pn5190__DataParams__t.html#accc6cc668c011832f8ad55b658b52e41", null ],
      [ "pInstrBuffer", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a8ce228abb8e81bcd71163fd3f5bb42a5", null ],
      [ "bNonRF_Cmd", "df/d78/structphhalHw__Pn5190__DataParams__t.html#af111f26abd6db88e2414eaeb93f21fd0", null ],
      [ "bCTSEvent", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a083f65f8f0a575994d39d320f1c06130", null ],
      [ "bRFONEvent", "df/d78/structphhalHw__Pn5190__DataParams__t.html#ade7c77ea4f0134135f2c78ab34895920", null ],
      [ "wAdditionalInfo", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a628e23e75695aa9a8baf330164ef5a19", null ],
      [ "dwEventStatus", "df/d78/structphhalHw__Pn5190__DataParams__t.html#ab7d3606f7bd250d968e56910d421d8c9", null ],
      [ "dwGenError", "df/d78/structphhalHw__Pn5190__DataParams__t.html#acf5651c0568a682112c3f34f3e251d19", null ],
      [ "dwEventReceived", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a7c62a4a3d510096c762f7db5625f006f", null ],
      [ "dwLpcdRefVal", "df/d78/structphhalHw__Pn5190__DataParams__t.html#af79af8d53fe4193c02734e1e3f6a28c5", null ],
      [ "bHFATTVal", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a3c9dfc5784ea346f0d7079d8624b7144", null ],
      [ "bNfcipMode", "df/d78/structphhalHw__Pn5190__DataParams__t.html#af5ff2c1fa28c6749888d46f19f4b0234", null ],
      [ "wCurrentSlotCount", "df/d78/structphhalHw__Pn5190__DataParams__t.html#aac9c6e786c22e3ca806c351ab15be550", null ],
      [ "dwExpectedEvent", "df/d78/structphhalHw__Pn5190__DataParams__t.html#a24d32797c24b468559a2596884cb9ec9", null ]
    ] ],
    [ "PHHAL_HW_PN5190_ID", "d3/dbf/group__phhalHw__Pn5190.html#gaa4cb63bf4bbafbfb70225ca52a755714", null ],
    [ "PHHAL_HW_PN5190_DEFAULT_TIMEOUT", "d3/dbf/group__phhalHw__Pn5190.html#ga34a75169ddead02db4b779ed5259e593", null ],
    [ "PHHAL_HW_PN5190_DEFAULT_TIMEOUT_MILLI", "d3/dbf/group__phhalHw__Pn5190.html#ga2f77e398e1e045dbe5bceeb196a59afd", null ],
    [ "PHHAL_HW_PN5190_SHADOW_COUNT", "d3/dbf/group__phhalHw__Pn5190.html#ga0154800065006f78fbc9ada359092e4d", null ],
    [ "INSTR_BUFFER_SIZE", "d3/dbf/group__phhalHw__Pn5190.html#gadf3e648673cad0cb642223488aaea044", null ],
    [ "INSTR_BUFFER_SIZE", "d3/dbf/group__phhalHw__Pn5190.html#gadf3e648673cad0cb642223488aaea044", null ],
    [ "MAX_ISR_READ_BUFFER_SIZE", "d3/dbf/group__phhalHw__Pn5190.html#ga133f453e1eabdb2cf7fef4ed78a465a2", null ],
    [ "PHHAL_HW_PN5190_DEFAULT_FELICA_EMD_REGISTER", "d3/dbf/group__phhalHw__Pn5190.html#ga64b8eadd87e5d56e6fba5c577eb8e6d6", null ],
    [ "PH_PN5190_EVT_RSP", "d3/dbf/group__phhalHw__Pn5190.html#ga280576bee781b4a8b3bc986ef2afb402", null ],
    [ "PH_PN5190_EVT_BOOT", "d3/dbf/group__phhalHw__Pn5190.html#ga8929d9c9165184c1acb238fd9772ca9f", null ],
    [ "PH_PN5190_EVT_GENERAL_ERROR", "d3/dbf/group__phhalHw__Pn5190.html#ga264b70c63431d49522141d0a150efbee", null ],
    [ "PH_PN5190_EVT_STANDBY_PREV", "d3/dbf/group__phhalHw__Pn5190.html#ga4f69abaa89e4964d43b80fa209cee84b", null ],
    [ "PH_PN5190_EVT_RFOFF_DETECT", "d3/dbf/group__phhalHw__Pn5190.html#ga0b9f2fe58780ad1a3f20c7a3dae16f6b", null ],
    [ "PH_PN5190_EVT_RFON_DETECT", "d3/dbf/group__phhalHw__Pn5190.html#ga113fd49b7cf7ee0fa9b385c52de4ea74", null ],
    [ "PH_PN5190_EVT_TX_OVERCURRENT_ERROR", "d3/dbf/group__phhalHw__Pn5190.html#ga919c3a61bf58470e604346c6384d045d", null ],
    [ "PH_PN5190_EVT_TIMER", "d3/dbf/group__phhalHw__Pn5190.html#ga361e72e9cd051ea41cf842f6f906e13f", null ],
    [ "PH_PN5190_EVT_AUTOCOLL", "d3/dbf/group__phhalHw__Pn5190.html#ga70d5423c959ecb3e62f24f2c63f712b5", null ],
    [ "PH_PN5190_EVT_LPCD", "d3/dbf/group__phhalHw__Pn5190.html#ga6d374d4ba353fd36ba327171ed08d082", null ],
    [ "PH_PN5190_EVT_LP_CALIB", "d3/dbf/group__phhalHw__Pn5190.html#gac1f7e9b41995c4ab1460b2b445f14898", null ],
    [ "PH_PN5190_EVT_IDLE", "d3/dbf/group__phhalHw__Pn5190.html#ga36a9b840d2088cea5b4d475376bd806f", null ],
    [ "PH_PN5190_EVT_CTS", "d3/dbf/group__phhalHw__Pn5190.html#ga8a75f9f6ca47015f021eb03a53bcbbad", null ],
    [ "PH_PN5190_EVT_RFU", "d3/dbf/group__phhalHw__Pn5190.html#ga92e048c33daef809ee788ee1915e3d0b", null ],
    [ "PH_ERR_PN5190_NO_CTS_EVENT", "d3/dbf/group__phhalHw__Pn5190.html#ga44653a38ddac05071ef976fd2edbf0c2", null ],
    [ "PHHAL_HW_PN5190_CONFIG_CTS_EVENT_STATUS", "d3/dbf/group__phhalHw__Pn5190.html#gae4f4ab269ce06cb980989386e05ea3c6", null ],
    [ "PHHAL_HW_PN5190_CONFIG_CTS_EVENT_ENABLE", "d3/dbf/group__phhalHw__Pn5190.html#ga0c45fad81821e33ff6812a9552ad1ded", null ],
    [ "PHHAL_HW_PN5190_CONFIG_LPCD_EVENT_ENABLE", "d3/dbf/group__phhalHw__Pn5190.html#gaf3e26de2ac1639cb5af2e65511086103", null ],
    [ "PHHAL_HW_PN5190_CONFIG_EXPECTED_EVENT", "d3/dbf/group__phhalHw__Pn5190.html#gac31d0e44199117d07bb803990001c669", null ],
    [ "PHHAL_HW_PN5190_CONFIG_RF_ON_EVENT_STATUS", "d3/dbf/group__phhalHw__Pn5190.html#gaf933f7a7b60a373fc24ae397c9508632", null ],
    [ "phhalHw_Pn5190_Init", "d3/dbf/group__phhalHw__Pn5190.html#gab06e16f76af662a200db823b00c0a9c9", null ],
    [ "phhalHw_Pn5190_SetListenParameters", "d3/dbf/group__phhalHw__Pn5190.html#gaec9a75f9f0e21a225ad0d22985ab897a", null ]
];
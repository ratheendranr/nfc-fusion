var structphalMful__Sw__DataParams__t =
[
    [ "wId", "d2/dda/structphalMful__Sw__DataParams__t.html#a62d774d92333c57a9bbb6e0ffb168a0e", null ],
    [ "pPalMifareDataParams", "d2/dda/structphalMful__Sw__DataParams__t.html#ac8ba41e622b609239ab388cece9912b7", null ],
    [ "pKeyStoreDataParams", "d2/dda/structphalMful__Sw__DataParams__t.html#a8d6e71dab621a32ca62413ecf4759064", null ],
    [ "pCryptoDataParams", "d2/dda/structphalMful__Sw__DataParams__t.html#a663895b0e42e01506e80dcee348b8f37", null ],
    [ "pCryptoRngDataParams", "d2/dda/structphalMful__Sw__DataParams__t.html#af6c7b67d2043daf2ad053b0d25d99ee8", null ],
    [ "bCMACReq", "d2/dda/structphalMful__Sw__DataParams__t.html#a3b60e3edfbddf045f32356ef936e6161", null ],
    [ "wCmdCtr", "d2/dda/structphalMful__Sw__DataParams__t.html#a2f458562ded5a2a6af5b8d0ca96472e8", null ],
    [ "bAuthMode", "d2/dda/structphalMful__Sw__DataParams__t.html#a02ea761ce22fcbe0eb7e68815c1a2265", null ],
    [ "bAdditionalInfo", "d2/dda/structphalMful__Sw__DataParams__t.html#a0bb43c249a2bd18fbf19fb84dadee7f6", null ]
];
var group__phalMfdfEVx__AppManagement =
[
    [ "PHAL_MFDFEVX_SELECT_PRIMARY_APP", "d6/df5/group__phalMfdfEVx__AppManagement.html#ga09fdcb69037af287f03ee5f4b58b4f98", null ],
    [ "PHAL_MFDFEVX_SELECT_SECOND_APP", "d6/df5/group__phalMfdfEVx__AppManagement.html#gad34f5cb22d20e607e597bcd9bbcc9a2c", null ],
    [ "phalMfdfEVx_CreateApplication", "d6/df5/group__phalMfdfEVx__AppManagement.html#ga9d796c7d2f2d284707346e15169aaac7", null ],
    [ "phalMfdfEVx_DeleteApplication", "d6/df5/group__phalMfdfEVx__AppManagement.html#ga8c4da7d2570ed868e65b28929b1e9c69", null ],
    [ "phalMfdfEVx_CreateDelegatedApplication", "d6/df5/group__phalMfdfEVx__AppManagement.html#ga48c4672a9f3f85cbb04541e029713b9e", null ],
    [ "phalMfdfEVx_SelectApplication", "d6/df5/group__phalMfdfEVx__AppManagement.html#gad6e1e97825148f9d4e8b4d88409a4772", null ],
    [ "phalMfdfEVx_GetApplicationIDs", "d6/df5/group__phalMfdfEVx__AppManagement.html#ga4da64f04d8fde8d04a740d6b62a3f32d", null ],
    [ "phalMfdfEVx_GetDelegatedInfo", "d6/df5/group__phalMfdfEVx__AppManagement.html#ga10a4e84046273f3ddc923888a6b72644", null ]
];
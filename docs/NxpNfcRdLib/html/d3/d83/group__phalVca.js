var group__phalVca =
[
    [ "Component : Software", "d5/db9/group__phalVca__Sw.html", "d5/db9/group__phalVca__Sw" ],
    [ "Virtual Card commands", "dc/da7/group__phalVca__VC.html", "dc/da7/group__phalVca__VC" ],
    [ "Proximity Check commands", "d0/d4f/group__phalVca__PC.html", "d0/d4f/group__phalVca__PC" ],
    [ "Utility", "d8/dc0/group__phalVca__Utilities.html", "d8/dc0/group__phalVca__Utilities" ],
    [ "PHAL_VCA_ERR_CMD_INVALID", "d3/d83/group__phalVca.html#gac38ce76c4d63d377eccf454aa2faab74", null ],
    [ "PHAL_VCA_ERR_FORMAT", "d3/d83/group__phalVca.html#ga013a8e1274d4d37488e41fee2c9d56c2", null ],
    [ "PHAL_VCA_ERR_AUTH", "d3/d83/group__phalVca.html#gaceca1fa10aba5db33a81980475840329", null ],
    [ "PHAL_VCA_ERR_GEN", "d3/d83/group__phalVca.html#ga9e9120dec0828592f9c3b6b4fc89fa6e", null ],
    [ "PHAL_VCA_ERR_CMD_OVERFLOW", "d3/d83/group__phalVca.html#gaa7e7e9fc0b449f382519df9847193d42", null ],
    [ "PHAL_VCA_ERR_COMMAND_ABORTED", "d3/d83/group__phalVca.html#gac4308aa91573e3b5e79f866a3bd8d0d3", null ],
    [ "PHAL_VCA_ERR_7816_GEN_ERROR", "d3/d83/group__phalVca.html#gaf591911b335ccbd012169da44f32e2a1", null ]
];
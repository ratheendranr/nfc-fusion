var group__phalMfpEVx__CommonDefs =
[
    [ "PHAL_MFPEVX_ISO14443_L3", "d3/d72/group__phalMfpEVx__CommonDefs.html#ga0b964abe53b0e246dfb6ba8d5bcda71c", null ],
    [ "PHAL_MFPEVX_ISO14443_L4", "d3/d72/group__phalMfpEVx__CommonDefs.html#ga6ed6fa14b0e7d88afae6de28f7df5e23", null ],
    [ "PHAL_MFPEVX_ENCRYPTION_OFF", "d3/d72/group__phalMfpEVx__CommonDefs.html#gaafb429a9e5ec221a3b43b7371d7b5b2c", null ],
    [ "PHAL_MFPEVX_ENCRYPTION_ON", "d3/d72/group__phalMfpEVx__CommonDefs.html#gacb3cf9a36fb271cba30cd12a93952b33", null ],
    [ "PHAL_MFPEVX_MAC_ON_COMMAND_OFF", "d3/d72/group__phalMfpEVx__CommonDefs.html#ga7b3d02f5b25ab068a854fc90542b10da", null ],
    [ "PHAL_MFPEVX_MAC_ON_COMMAND_ON", "d3/d72/group__phalMfpEVx__CommonDefs.html#gaa42ed769f657d9c1b42f56171f5ec06d", null ],
    [ "PHAL_MFPEVX_MAC_ON_RESPONSE_OFF", "d3/d72/group__phalMfpEVx__CommonDefs.html#gadbffe068f2229de390e86b63ccc92f0b", null ],
    [ "PHAL_MFPEVX_MAC_ON_RESPONSE_ON", "d3/d72/group__phalMfpEVx__CommonDefs.html#ga229ba9669989b5e6af8f8a5340fea1f9", null ]
];
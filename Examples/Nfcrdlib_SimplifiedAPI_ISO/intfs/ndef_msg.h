/*
 * ndef_msg.h
 *
 *  Created on: Apr. 10, 2021
 *      Author: pankil
 */

#ifndef NDEF_MSG_H_
#define NDEF_MSG_H_

#include <stdint.h>
#include <phApp_Init.h>

#define MAX_NDEF_RECORD		5
#define MFUL_READ_SIZE 		16

#define	NETWORK_KEY_FIELD_ID	0x1027
#define MAC_ADDRESS_ID 			0x1020
#define AUTH_TYPE_FIELD_ID	    0x1003
#define ENCRYPTION_TYPE_ID      0x100F
#define AUTH_TYPE_OPEN          0x0001
#define AUTH_TYPE_WPA_PSK       0x0002
#define AUTH_TYPE_WPA_EAP       0x0008
#define AUTH_TYPE_WPA2_EAP      0x0010
#define AUTH_TYPE_WPA2_PSK      0x0020
#define ENCRYPT_TYPE_NONE       0x0001
#define ENCRYPT_TYPE_WEP        0x0002
#define ENCRYPT_TYPE_TKIP       0x0004
#define ENCRYPT_TYPE_AES        0x0008
#define ENCRYPT_TYPE_MMODE      0x000c   //AES/TKIP Mixed Mode


//NDEF record has information about record type, record id and record payload
typedef struct{
	char *payload_type_value;
	char *payload_id_value;
	char *record_value;
}ndef_record_value_t;

//NDEF record first byte
typedef struct{
	uint8_t TNF;
	uint8_t IL;
	uint8_t SR;
	uint8_t CF;
	uint8_t ME;
	uint8_t MB;
}record_byte_1_t;

//NDEF messge can have multiple records
//Max records we are supporting here is 5
typedef struct{
	record_byte_1_t 	first_byte;
	uint8_t 			type_length;
	uint32_t			payload_length;
	uint8_t				id_length;
	uint8_t				payload_type;
	uint8_t 			payload_id;
	ndef_record_value_t		record_payload;
}ndef_record_t;


typedef struct{
	uint8_t			first_page;
	int8_t 			msg_length;
	uint8_t 		msg_start_idx;
	uint16_t		buffer_size;
	uint16_t		tag_capacity;
	uint8_t			ndef_msg_data[1024];
	uint8_t			record_count;
	ndef_record_t	ndef_msg[MAX_NDEF_RECORD];
}ndef_msg_ctx_t;


//uint32_t NfcFindNdefMsg(uint8_t tag_type);

#endif //NDEF_MSG_H_
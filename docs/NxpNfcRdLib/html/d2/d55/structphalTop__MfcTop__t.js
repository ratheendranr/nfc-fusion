var structphalTop__MfcTop__t =
[
    [ "pPalI14443paDataParams", "d2/d55/structphalTop__MfcTop__t.html#a890836c013e0e2c5bcbbdb6f18bdd5b5", null ],
    [ "bCardType", "d2/d55/structphalTop__MfcTop__t.html#a90d26f253a5a1d4dc31be1f3ee983fe3", null ],
    [ "bFirstNdefSector", "d2/d55/structphalTop__MfcTop__t.html#aa22f3f5064c71634b8031654c4106fa1", null ],
    [ "bPreFormatted", "d2/d55/structphalTop__MfcTop__t.html#ad0e8829a686b5f3f90060b6c66c1c829", null ],
    [ "bNdefSectorCount", "d2/d55/structphalTop__MfcTop__t.html#af920e4399124db5fc7d30815b7a22808", null ],
    [ "bOffset", "d2/d55/structphalTop__MfcTop__t.html#ac5382dc8d5dcf5171cf7ac1181db4a63", null ],
    [ "bNdefMessageStart", "d2/d55/structphalTop__MfcTop__t.html#a2c6ffcd05b41e1f1d75da57cc424b655", null ]
];
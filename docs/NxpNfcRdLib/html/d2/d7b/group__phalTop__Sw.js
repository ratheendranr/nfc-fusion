var group__phalTop__Sw =
[
    [ "phalTop_T1T_LockCtrlTlv_t", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html", [
      [ "wOffset", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a089bcdf110e83f03e237725784cb8a11", null ],
      [ "wByteAddr", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a76aeb5b8e9508bd0d1bc349ea753d70a", null ],
      [ "bSizeInBits", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a0793fd80d874a1274a63334cf51b534a", null ],
      [ "bBytesPerPage", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a31e9e503fbb8b90e7dda4096aaf43c8c", null ],
      [ "bBytesLockedPerBit", "d0/d05/structphalTop__T1T__LockCtrlTlv__t.html#a2b84e12c70d2417b330a1f9ce7f256f3", null ]
    ] ],
    [ "phalTop_T1T_MemCtrlTlv_t", "d0/d4b/structphalTop__T1T__MemCtrlTlv__t.html", [
      [ "wOffset", "d0/d4b/structphalTop__T1T__MemCtrlTlv__t.html#ab2bf5a4f01d3297622c4effd378d8fa9", null ],
      [ "wByteAddr", "d0/d4b/structphalTop__T1T__MemCtrlTlv__t.html#a17d22cf6e49a4abb8ccc2bdb9add5351", null ],
      [ "bSizeInBytes", "d0/d4b/structphalTop__T1T__MemCtrlTlv__t.html#a07152ca7858dabb6bda66d6ca2bedca7", null ],
      [ "bBytesPerPage", "d0/d4b/structphalTop__T1T__MemCtrlTlv__t.html#aab7d7d5943c6bd27edc7ba7afaf8d53f", null ]
    ] ],
    [ "phalTop_T1T_ProprietaryTlv_t", "dd/dc8/structphalTop__T1T__ProprietaryTlv__t.html", [
      [ "wOffset", "dd/dc8/structphalTop__T1T__ProprietaryTlv__t.html#afdc6eb02e29d289dc9a3d89efcb583a1", null ],
      [ "wLength", "dd/dc8/structphalTop__T1T__ProprietaryTlv__t.html#ad0f484532ee112433fde41dfb57ebb5e", null ]
    ] ],
    [ "phalTop_TIT_Segment_t", "d7/d19/structphalTop__TIT__Segment__t.html", [
      [ "bAddress", "d7/d19/structphalTop__TIT__Segment__t.html#a7cba951eac901556752c286e5d63e566", null ],
      [ "pData", "d7/d19/structphalTop__TIT__Segment__t.html#ad3b0949ef39b29be3e70d4b5b5edd989", null ],
      [ "bLockReservedOtp", "d7/d19/structphalTop__TIT__Segment__t.html#a16bac46e14007b7ee036d6bed696f51b", null ]
    ] ],
    [ "phalTop_T2T_LockCtrlTlv_t", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html", [
      [ "wOffset", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#ad8bc59b76a5a54f03070b33dae40d032", null ],
      [ "wByteAddr", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#ae4579c81e8e24cf2d55b68b2c766fd5b", null ],
      [ "bSizeInBits", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#aa36af0441952d4fa471a848bbbf47e77", null ],
      [ "bBytesPerPage", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#a795e9766456edf504e6f7999977ead85", null ],
      [ "bBytesLockedPerBit", "d8/d94/structphalTop__T2T__LockCtrlTlv__t.html#af077c6cc686b7cc7a91190e98bc0d8c7", null ]
    ] ],
    [ "phalTop_T2T_MemCtrlTlv_t", "d8/d71/structphalTop__T2T__MemCtrlTlv__t.html", [
      [ "wOffset", "d8/d71/structphalTop__T2T__MemCtrlTlv__t.html#add1dabbbaa90f102128743b63d3e9269", null ],
      [ "wByteAddr", "d8/d71/structphalTop__T2T__MemCtrlTlv__t.html#aa77e5ae9d85a62c71ce6e663e30398a4", null ],
      [ "bSizeInBytes", "d8/d71/structphalTop__T2T__MemCtrlTlv__t.html#a1b42d5ae4134a96218e90b8252fb3ed2", null ],
      [ "bBytesPerPage", "d8/d71/structphalTop__T2T__MemCtrlTlv__t.html#a476c059db5fa707ca41801b75989ac44", null ]
    ] ],
    [ "phalTop_T2T_Sector_t", "d5/d3d/structphalTop__T2T__Sector__t.html", [
      [ "bAddress", "d5/d3d/structphalTop__T2T__Sector__t.html#a16a46b3bd42b403c9cea76dd11a1c2ba", null ],
      [ "bBlockAddress", "d5/d3d/structphalTop__T2T__Sector__t.html#ab903a84d8e2a2d709ebec7b53e0ad3cb", null ],
      [ "bLockReservedOtp", "d5/d3d/structphalTop__T2T__Sector__t.html#a1337de8219ec87176e703e223b6eb623", null ],
      [ "bValidity", "d5/d3d/structphalTop__T2T__Sector__t.html#a54e2c3e2c7f105bde9b178d80083000d", null ]
    ] ],
    [ "phalTop_T1T_t", "dc/d03/structphalTop__T1T__t.html", [
      [ "pAlT1TDataParams", "dc/d03/structphalTop__T1T__t.html#a17ff930c804550a23bc22bfe43aca873", null ],
      [ "bRwa", "dc/d03/structphalTop__T1T__t.html#a9cfba78bc85482d678de3c094fbbff38", null ],
      [ "bTms", "dc/d03/structphalTop__T1T__t.html#a6876ca035b71880ae3fd47c42c93d09b", null ],
      [ "bTagMemoryType", "dc/d03/structphalTop__T1T__t.html#a6b05e6c88fcca31b2e32d3ae5162ef52", null ],
      [ "bTerminatorTlvPresence", "dc/d03/structphalTop__T1T__t.html#a180367b9ac447f4fa3db7a57e5ab5c8d", null ],
      [ "bLockTlvCount", "dc/d03/structphalTop__T1T__t.html#aec81a0b777b7ee0898c6dd24b198007c", null ],
      [ "bMemoryTlvCount", "dc/d03/structphalTop__T1T__t.html#a3f93eabade8709c0ff57091efe215c5f", null ],
      [ "wNdefHeaderAddr", "dc/d03/structphalTop__T1T__t.html#a8b5f58ebe39b8be13a48c83fd84be132", null ],
      [ "wNdefMsgAddr", "dc/d03/structphalTop__T1T__t.html#a4dcf93bfa216e5fc26e8210f8347fa36", null ],
      [ "bUid", "dc/d03/structphalTop__T1T__t.html#a5dcab06c5a157c9fee4c944f6037774c", null ],
      [ "asMemCtrlTlv", "dc/d03/structphalTop__T1T__t.html#a0f958666448ed38e80cd7fe6a09ffedb", null ],
      [ "asLockCtrlTlv", "dc/d03/structphalTop__T1T__t.html#a459e29d121929b5ca56eead09101068f", null ],
      [ "sSegment", "dc/d03/structphalTop__T1T__t.html#ab647a25dafa49fd2659abd8204b9ab00", null ]
    ] ],
    [ "phalTop_T2T_t", "d3/d23/structphalTop__T2T__t.html", [
      [ "pAlT2TDataParams", "d3/d23/structphalTop__T2T__t.html#ae02f53fc4a1d442422e2832f0822aa41", null ],
      [ "bRwa", "d3/d23/structphalTop__T2T__t.html#ae1f7c592bc94fad34fc741397b2d3ee9", null ],
      [ "bTms", "d3/d23/structphalTop__T2T__t.html#a15be3399e0d591f6a42f278a834d9582", null ],
      [ "bTagMemoryType", "d3/d23/structphalTop__T2T__t.html#a7a8a74857fff4773b5250aceecee54dc", null ],
      [ "bLockTlvCount", "d3/d23/structphalTop__T2T__t.html#a61ae168d4b695b8f84751162b093bd0f", null ],
      [ "bMemoryTlvCount", "d3/d23/structphalTop__T2T__t.html#a700fe640358f8aafb08bee63717a2eb0", null ],
      [ "wNdefHeaderAddr", "d3/d23/structphalTop__T2T__t.html#a5de7dd838ba16373da444ef2634fec24", null ],
      [ "wNdefMsgAddr", "d3/d23/structphalTop__T2T__t.html#ac23daa62e744ddc8740218402e1e20db", null ],
      [ "asMemCtrlTlv", "d3/d23/structphalTop__T2T__t.html#ae6fcdfb75a87d5c224b8ca77dcef4c35", null ],
      [ "asLockCtrlTlv", "d3/d23/structphalTop__T2T__t.html#a128438768986161d44105de0f9a474d2", null ],
      [ "sSector", "d3/d23/structphalTop__T2T__t.html#a58a40e2d710b5430feab415ed417d702", null ]
    ] ],
    [ "phalTop_T3T_t", "d3/d12/structphalTop__T3T__t.html", [
      [ "pAlT3TDataParams", "d3/d12/structphalTop__T3T__t.html#aaa29bbf667f5b18df2bb9a3273003dcf", null ],
      [ "bRwa", "d3/d12/structphalTop__T3T__t.html#a76180e8fdd5b75c1e20a108dd262810d", null ],
      [ "bNbr", "d3/d12/structphalTop__T3T__t.html#a9e3ccbd70265de71727fa3b77cc75843", null ],
      [ "bNbw", "d3/d12/structphalTop__T3T__t.html#a4e735984cbfc557f260f685f5b3f82a3", null ],
      [ "bNmaxb", "d3/d12/structphalTop__T3T__t.html#a67d1ba2914863643bb17af8703194a20", null ],
      [ "bUid", "d3/d12/structphalTop__T3T__t.html#a355df6283d73ae0f74f5b266499fa723", null ],
      [ "bAttributeBlock", "d3/d12/structphalTop__T3T__t.html#af7a47370de37cf2f6d3a639c3ac29550", null ]
    ] ],
    [ "phalTop_T4T_t", "d1/d6b/structphalTop__T4T__t.html", [
      [ "pAlT4TDataParams", "d1/d6b/structphalTop__T4T__t.html#aa9e95372693c9a7ef009e6530b235267", null ],
      [ "aNdefFileID", "d1/d6b/structphalTop__T4T__t.html#a7ec07f3bf3706b4c5aa85d1eec4d4eea", null ],
      [ "bRa", "d1/d6b/structphalTop__T4T__t.html#a91598212a9f051e28dbbe16a0d8c0e7f", null ],
      [ "bWa", "d1/d6b/structphalTop__T4T__t.html#a06ebea5cd47b0c5a220271ff5c38d243", null ],
      [ "bCurrentSelectedFile", "d1/d6b/structphalTop__T4T__t.html#aabd7a4f7b638c173d520d5c9dfab89d6", null ],
      [ "wMLe", "d1/d6b/structphalTop__T4T__t.html#abb05ccfdd0b9082533804db7c6973d12", null ],
      [ "wMLc", "d1/d6b/structphalTop__T4T__t.html#ac3666b57768ff1c8473ad01f5bf781c2", null ],
      [ "wCCLEN", "d1/d6b/structphalTop__T4T__t.html#ae9346092dd490c9a5e5aba179cfcf7a0", null ],
      [ "wMaxFileSize", "d1/d6b/structphalTop__T4T__t.html#a8e80e6a0d697fb71838b3453ea49fd2f", null ]
    ] ],
    [ "phalTop_T5T_t", "da/dec/structphalTop__T5T__t.html", [
      [ "pAlI15693DataParams", "da/dec/structphalTop__T5T__t.html#a92f532872675980fbf48657780318224", null ],
      [ "bRwa", "da/dec/structphalTop__T5T__t.html#aa4d887147dd1134e38c437107b38b190", null ],
      [ "bTerminatorTlvPresence", "da/dec/structphalTop__T5T__t.html#aca70e25407302090f72a423fc1ebb126", null ],
      [ "bMbRead", "da/dec/structphalTop__T5T__t.html#a1460e1d74c00bf6412deedf2a987fca0", null ],
      [ "bLockBlock", "da/dec/structphalTop__T5T__t.html#a2a8578a5d857db47ac533cd7dece715b", null ],
      [ "bSplFrm", "da/dec/structphalTop__T5T__t.html#aa7b2ce069ced5b5234370a6237724dbc", null ],
      [ "bExtendedCommandSupport", "da/dec/structphalTop__T5T__t.html#ab9a994b91cda6e2c14ad6c13e5970456", null ],
      [ "bOptionFlag", "da/dec/structphalTop__T5T__t.html#a7bdfc4578018f3de74d304a8da04e1a5", null ],
      [ "wMlen", "da/dec/structphalTop__T5T__t.html#a02a1c82a311f0b8e72e0cc0c3f06f30f", null ],
      [ "wNdefHeaderAddr", "da/dec/structphalTop__T5T__t.html#a4ffad20ab0fd67093576f0f520e60cfa", null ],
      [ "wNdefMsgAddr", "da/dec/structphalTop__T5T__t.html#a9ff455eb842b3f6ec4891c6555b885df", null ],
      [ "bBlockSize", "da/dec/structphalTop__T5T__t.html#a875a8fbaf9874540f83e9a6045547912", null ],
      [ "bMaxBlockNum", "da/dec/structphalTop__T5T__t.html#aa6e71bbe1c3f806590083b1653448b23", null ]
    ] ],
    [ "phalTop_MfcTop_t", "d2/d55/structphalTop__MfcTop__t.html", [
      [ "pPalI14443paDataParams", "d2/d55/structphalTop__MfcTop__t.html#a890836c013e0e2c5bcbbdb6f18bdd5b5", null ],
      [ "bCardType", "d2/d55/structphalTop__MfcTop__t.html#a90d26f253a5a1d4dc31be1f3ee983fe3", null ],
      [ "bFirstNdefSector", "d2/d55/structphalTop__MfcTop__t.html#aa22f3f5064c71634b8031654c4106fa1", null ],
      [ "bPreFormatted", "d2/d55/structphalTop__MfcTop__t.html#ad0e8829a686b5f3f90060b6c66c1c829", null ],
      [ "bNdefSectorCount", "d2/d55/structphalTop__MfcTop__t.html#af920e4399124db5fc7d30815b7a22808", null ],
      [ "bOffset", "d2/d55/structphalTop__MfcTop__t.html#ac5382dc8d5dcf5171cf7ac1181db4a63", null ],
      [ "bNdefMessageStart", "d2/d55/structphalTop__MfcTop__t.html#a2c6ffcd05b41e1f1d75da57cc424b655", null ]
    ] ],
    [ "phalTop_Sw_DataParams_t", "da/ded/structphalTop__Sw__DataParams__t.html", [
      [ "wId", "da/ded/structphalTop__Sw__DataParams__t.html#a92883f700fdc13962a398cf3f862ef4f", null ],
      [ "bTagType", "da/ded/structphalTop__Sw__DataParams__t.html#a2dcc6cb9e10c275ebe76c019c975472c", null ],
      [ "bVno", "da/ded/structphalTop__Sw__DataParams__t.html#afda57ce1c16346e571838065fcfdbaf8", null ],
      [ "bTagState", "da/ded/structphalTop__Sw__DataParams__t.html#a493b602e7dde8c0068f98b0f0da36ea6", null ],
      [ "wNdefLength", "da/ded/structphalTop__Sw__DataParams__t.html#a38314d5452faf42fb47029be9750f024", null ],
      [ "wMaxNdefLength", "da/ded/structphalTop__Sw__DataParams__t.html#a9eeaad70bc49797b2e24e4d27c588440", null ],
      [ "pTopTagsDataParams", "da/ded/structphalTop__Sw__DataParams__t.html#a4280a4e499248ddda335295a6ed810bb", null ],
      [ "salTop_T1T", "da/ded/structphalTop__Sw__DataParams__t.html#a4c2fe911a779325fb7cda6b28cdfce49", null ],
      [ "salTop_T2T", "da/ded/structphalTop__Sw__DataParams__t.html#ab1ef29d6e6f505b2de0f7aa9002dfbed", null ],
      [ "salTop_T3T", "da/ded/structphalTop__Sw__DataParams__t.html#a924fe599742c4335c794c3642648916a", null ],
      [ "salTop_T4T", "da/ded/structphalTop__Sw__DataParams__t.html#a9a780abd675bee91272cf81399b3c7f1", null ],
      [ "salTop_T5T", "da/ded/structphalTop__Sw__DataParams__t.html#a3602685432378bcf3192d4138ef3f902", null ],
      [ "salTop_MfcTop", "da/ded/structphalTop__Sw__DataParams__t.html#a329dc24f9c56f2e803081ec5fd9e757f", null ]
    ] ],
    [ "PHAL_TOP_SW_ID", "d2/d7b/group__phalTop__Sw.html#gab0ec64e7943e87301e2bed113084c9cd", null ],
    [ "PHAL_TOP_MAX_TAGTYPE_SUPPORTED", "d2/d7b/group__phalTop__Sw.html#ga5bc6a3ebad042cd6168ce0e91ae48a6a", null ],
    [ "PHAL_TOP_T1T_MAX_MEM_CTRL_TLV", "d2/d7b/group__phalTop__Sw.html#ga0f28e179a019e26d3be5c0bdb9fb2572", null ],
    [ "PHAL_TOP_T1T_MAX_LOCK_CTRL_TLV", "d2/d7b/group__phalTop__Sw.html#ga464bb2ea30c3d10b950a15a96094774d", null ],
    [ "PHAL_TOP_T2T_MAX_MEM_CTRL_TLV", "d2/d7b/group__phalTop__Sw.html#ga62f2bd9f6285f6875bee63c09f9d779c", null ],
    [ "PHAL_TOP_T2T_MAX_LOCK_CTRL_TLV", "d2/d7b/group__phalTop__Sw.html#gad62d982e89c54b5e255c68a7cb7c8db5", null ],
    [ "PHAL_TOP_T3T_READ_MAX_BLOCKS", "d2/d7b/group__phalTop__Sw.html#gaa3aee85e57a1dcce6d74ac8a695a525e", null ],
    [ "PHAL_TOP_T3T_WRITE_MAX_BLOCKS", "d2/d7b/group__phalTop__Sw.html#gac5e563eb171c74212ff0b0c1e1e9dd84", null ],
    [ "PHAL_TOP_T1T_NDEF_SUPPORTED_VNO", "d2/d7b/group__phalTop__Sw.html#gaadcc31d7f29acae6a7874ba8cb7ff477", null ],
    [ "PHAL_TOP_T2T_NDEF_SUPPORTED_VNO", "d2/d7b/group__phalTop__Sw.html#gaf775ca089a2d3ce81955710495bfa63f", null ],
    [ "PHAL_TOP_T3T_NDEF_SUPPORTED_VNO", "d2/d7b/group__phalTop__Sw.html#gaa4cded92d94573ea730da3ac1ddd83c3", null ],
    [ "PHAL_TOP_T4T_NDEF_SUPPORTED_VNO", "d2/d7b/group__phalTop__Sw.html#ga0ebb7e819515d3b73c4d999587b348dc", null ],
    [ "PHAL_TOP_T5T_NDEF_SUPPORTED_VNO", "d2/d7b/group__phalTop__Sw.html#gaa286f08cf28cf689a3191f80453366e2", null ],
    [ "PHAL_TOP_ERR_READONLY_TAG", "d2/d7b/group__phalTop__Sw.html#ga7e15f57b6d75630023454a6c5b75e38d", null ],
    [ "PHAL_TOP_ERR_INVALID_STATE", "d2/d7b/group__phalTop__Sw.html#gafb6408a9065e2e02c4a760510026f68b", null ],
    [ "PHAL_TOP_ERR_FORMATTED_TAG", "d2/d7b/group__phalTop__Sw.html#ga804ff038cadfb8458dd5f391c3ba67bd", null ],
    [ "PHAL_TOP_ERR_UNSUPPORTED_VERSION", "d2/d7b/group__phalTop__Sw.html#ga150c17cdec38721f7fd9e995258176e4", null ],
    [ "PHAL_TOP_ERR_MISCONFIGURED_TAG", "d2/d7b/group__phalTop__Sw.html#ga7c9520a988bf83f4ce6afc4f4b3e624e", null ],
    [ "PHAL_TOP_ERR_UNSUPPORTED_TAG", "d2/d7b/group__phalTop__Sw.html#ga8dfd330269d79ba4e4156b3df30b87e7", null ],
    [ "PHAL_TOP_ERR_EMPTY_NDEF", "d2/d7b/group__phalTop__Sw.html#gad1438a9b5d6bc3089dfcb96557bf133f", null ],
    [ "PHAL_TOP_ERR_NON_NDEF_TAG", "d2/d7b/group__phalTop__Sw.html#ga2f165e9645388a0ed85485ee732c89d7", null ],
    [ "PHAL_TOP_NO_MFC", "d2/d7b/group__phalTop__Sw.html#ga63a97dad943e4c1bc134d78ffe2d9796", null ],
    [ "PHAL_TOP_MFC_1K", "d2/d7b/group__phalTop__Sw.html#ga5f956482a36741951a4ccab1cd77b50f", null ],
    [ "PHAL_TOP_MFC_4K", "d2/d7b/group__phalTop__Sw.html#gad2c7bf722e616352323e5419b62777b8", null ],
    [ "PHAL_TOP_MFP_2K", "d2/d7b/group__phalTop__Sw.html#gaf696ec1d8393bd9d76cf165f0f9a9a4f", null ],
    [ "PHAL_TOP_MFP_4K", "d2/d7b/group__phalTop__Sw.html#gab642d9d6117cf55aa56d18f87d0c9108", null ],
    [ "phalTop_Sw_Init", "d2/d7b/group__phalTop__Sw.html#ga8dbcf5b4682ff1c2b12c1026a74458fa", null ]
];
var group__phalMfdfEVx__TransactionnManagement =
[
    [ "PHAL_MFDFEVX_COMMIT_TXN_OPTION_NOT_EXCHANGED", "d8/d4a/group__phalMfdfEVx__TransactionnManagement.html#gac74ab0c6e00f1e218a14a39e3f45f5a3", null ],
    [ "PHAL_MFDFEVX_COMMIT_TXN_NO_TMC_TMV_RETURNED", "d8/d4a/group__phalMfdfEVx__TransactionnManagement.html#ga0145dba3b68f4dc6aeccf9ee284cd1b9", null ],
    [ "PHAL_MFDFEVX_COMMIT_TXN_TMC_TMV_RETURNED", "d8/d4a/group__phalMfdfEVx__TransactionnManagement.html#gaa8febcd3947be8119894487ced959756", null ],
    [ "phalMfdfEVx_CommitTransaction", "d8/d4a/group__phalMfdfEVx__TransactionnManagement.html#gaf17883e699b442884d47f8ecf21b6962", null ],
    [ "phalMfdfEVx_AbortTransaction", "d8/d4a/group__phalMfdfEVx__TransactionnManagement.html#gafcbeaa7a81bd76d73ae64e02fa3cc140", null ],
    [ "phalMfdfEVx_CommitReaderID", "d8/d4a/group__phalMfdfEVx__TransactionnManagement.html#gad9554a99c1a373606baca265f9463ccd", null ]
];
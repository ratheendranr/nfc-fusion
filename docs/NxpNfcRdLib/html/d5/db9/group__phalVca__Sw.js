var group__phalVca__Sw =
[
    [ "phalVca_Sw_CardTableEntry_t", "d9/daf/structphalVca__Sw__CardTableEntry__t.html", [
      [ "wIidIndex", "d9/daf/structphalVca__Sw__CardTableEntry__t.html#ad753a77aae4b02f385f8d1c6fdd6fd3f", null ],
      [ "bValid", "d9/daf/structphalVca__Sw__CardTableEntry__t.html#ab604f241e3d36bb759efde5ef83eabb5", null ],
      [ "pCardData", "d9/daf/structphalVca__Sw__CardTableEntry__t.html#a7c2ba4dce5f619b2e645a41da0f361c9", null ]
    ] ],
    [ "phalVca_Sw_IidTableEntry_t", "df/dc3/structphalVca__Sw__IidTableEntry__t.html", [
      [ "wIidIndex", "df/dc3/structphalVca__Sw__IidTableEntry__t.html#a75eaba5b4395488f4c4f159faf670ee6", null ],
      [ "wKeyEncNumber", "df/dc3/structphalVca__Sw__IidTableEntry__t.html#a16334d5263c6cde5d40249dbaac85e34", null ],
      [ "wKeyEncVersion", "df/dc3/structphalVca__Sw__IidTableEntry__t.html#ad55787e410da625003f70a10dd6331ba", null ],
      [ "wKeyMacNumber", "df/dc3/structphalVca__Sw__IidTableEntry__t.html#a705085269596369bcc6592d0e4bb449c", null ],
      [ "wKeyMacVersion", "df/dc3/structphalVca__Sw__IidTableEntry__t.html#a7b47611dda0388b0ec46fc3a7717bcf3", null ]
    ] ],
    [ "phalVca_Sw_DataParams_t", "df/d56/structphalVca__Sw__DataParams__t.html", [
      [ "wId", "df/d56/structphalVca__Sw__DataParams__t.html#aa0d6558483263bfc30964670290da61d", null ],
      [ "pPalMifareDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#a0e3d2e2ac96ab198c9b4ee91cdac5070", null ],
      [ "pKeyStoreDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#ad5b4d4b082953e68bf23d407f20d3102", null ],
      [ "pCryptoDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#a763cad67213cde29e0c929dba74772bc", null ],
      [ "pCryptoRngDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#a89568d21e4e117533b9874b6ba1db85d", null ],
      [ "pCardTable", "df/d56/structphalVca__Sw__DataParams__t.html#aaa2f41ca8e1512495ec5a2c3cc084303", null ],
      [ "pIidTable", "df/d56/structphalVca__Sw__DataParams__t.html#acdb19cd829102c52bf98d02ca49e2e55", null ],
      [ "wCurrentCardTablePos", "df/d56/structphalVca__Sw__DataParams__t.html#a07cf77633b3fa2176694343c8a54373b", null ],
      [ "wNumCardTableEntries", "df/d56/structphalVca__Sw__DataParams__t.html#a1d0145c775ccc682988230704ac38217", null ],
      [ "wNumIidTableEntries", "df/d56/structphalVca__Sw__DataParams__t.html#a5e389f853d358b7a2ff245fb1ca71d89", null ],
      [ "wCurrentIidIndex", "df/d56/structphalVca__Sw__DataParams__t.html#aebecabe16d3b95efa56c0da52b776bb8", null ],
      [ "wCurrentIidTablePos", "df/d56/structphalVca__Sw__DataParams__t.html#afb54dc5ebd4733e262be2a63f41969cc", null ],
      [ "wAdditionalInfo", "df/d56/structphalVca__Sw__DataParams__t.html#a67e9113c9d0fca83255ea15526119324", null ],
      [ "bSessionAuthMACKey", "df/d56/structphalVca__Sw__DataParams__t.html#af80c51c2ec7b627a8d7b5b389a1708ff", null ],
      [ "eVCState", "df/d56/structphalVca__Sw__DataParams__t.html#a2d91c2b33845c41e8b6e5aec2887ba77", null ],
      [ "ePCState", "df/d56/structphalVca__Sw__DataParams__t.html#ae4bb8898a4fd0b767c65d08cb5f1b7b9", null ],
      [ "pAlDataParams", "df/d56/structphalVca__Sw__DataParams__t.html#a487770466acb618bb75efb835880210c", null ],
      [ "bWrappedMode", "df/d56/structphalVca__Sw__DataParams__t.html#a926afe433187903a59cadef7ed625fb1", null ],
      [ "bExtendedLenApdu", "df/d56/structphalVca__Sw__DataParams__t.html#ae6d537197aeffdcc0f307f272d2c05e5", null ],
      [ "bOption", "df/d56/structphalVca__Sw__DataParams__t.html#a721bcbd8fda5fcbfb4ca3d2492c6db45", null ],
      [ "bLowerBoundThreshold", "df/d56/structphalVca__Sw__DataParams__t.html#aac24673dafb92ae329739919b64b9a08", null ]
    ] ],
    [ "PHAL_VCA_SW_ID", "d5/db9/group__phalVca__Sw.html#gaba050a186b6335a303bd61f12f47309c", null ],
    [ "phalVca_VirtualCardState", "d5/db9/group__phalVca__Sw.html#ga961e387535c3e37abf9294ae356a2506", [
      [ "VC_NOT_SELECTED", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a94d0201a112f4358755a63ccb81f6f8c", null ],
      [ "VC_PROXIMITYFAILED", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a4f9db8baa93dbd0410ca2a1abebfa3e5", null ],
      [ "VC_PROXIMITYCHECK", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a888092da11b166034ae5ec81c3d08bab", null ],
      [ "VC_DF_NOT_AUTH", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a1c2178c53a9144ebe05b428d8580e224", null ],
      [ "VC_DF_AUTH_D40", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a9ba397c00c15301c7e4d099a16fd9902", null ],
      [ "VC_DF_AUTH_ISO", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a69ca6d324df95cf0e81b89cb716bbd1b", null ],
      [ "VC_DF_AUTH_EV2", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a08f67826d3a0e8aab95ea70d32b1f0f0", null ],
      [ "VC_DF_AUTH_AES", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a9a2c9d7201f20802b83eba03a097dcfe", null ],
      [ "VC_MFP_AUTH_AES_SL1", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a17e8e64fde90d8ee0abfa4e0b1e9c0cb", null ],
      [ "VC_MFP_AUTH_AES_SL3", "d5/db9/group__phalVca__Sw.html#gga961e387535c3e37abf9294ae356a2506a2eff620b6b58e3137ae830267f5a6c49", null ]
    ] ],
    [ "phalVca_ProximityCheckState", "d5/db9/group__phalVca__Sw.html#gae0f14b2a9ea1ce160c6411674613162b", [
      [ "PC_NO_PCHK_IN_PROGRESS", "d5/db9/group__phalVca__Sw.html#ggae0f14b2a9ea1ce160c6411674613162ba07ddbe0de5222524c074fdb67520a79e", null ],
      [ "PC_PPC_IN_PROGRESS", "d5/db9/group__phalVca__Sw.html#ggae0f14b2a9ea1ce160c6411674613162babcc4dc5ef3c4087c32c52978b0063360", null ],
      [ "PC_PCHK_PREPARED", "d5/db9/group__phalVca__Sw.html#ggae0f14b2a9ea1ce160c6411674613162ba7d079d870d40343fcf833ce79a2babcd", null ],
      [ "PC_PCHK_IN_PROGRESS", "d5/db9/group__phalVca__Sw.html#ggae0f14b2a9ea1ce160c6411674613162bad1667c95aef3060d28ed6dbbf185163e", null ],
      [ "PC_WAITING_PC_VERIFICATION", "d5/db9/group__phalVca__Sw.html#ggae0f14b2a9ea1ce160c6411674613162bae4fff15994c6630aa8c6e2b32be5dfef", null ],
      [ "PC_VPC_IN_PROGRESS", "d5/db9/group__phalVca__Sw.html#ggae0f14b2a9ea1ce160c6411674613162ba5d60a8d64d320b658e0aceb15265c166", null ]
    ] ],
    [ "phalVca_Sw_Init", "d5/db9/group__phalVca__Sw.html#ga1a7bac102a4e6ee0f22d8e0c953dbba0", null ]
];
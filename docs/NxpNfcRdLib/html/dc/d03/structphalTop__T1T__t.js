var structphalTop__T1T__t =
[
    [ "pAlT1TDataParams", "dc/d03/structphalTop__T1T__t.html#a17ff930c804550a23bc22bfe43aca873", null ],
    [ "bRwa", "dc/d03/structphalTop__T1T__t.html#a9cfba78bc85482d678de3c094fbbff38", null ],
    [ "bTms", "dc/d03/structphalTop__T1T__t.html#a6876ca035b71880ae3fd47c42c93d09b", null ],
    [ "bTagMemoryType", "dc/d03/structphalTop__T1T__t.html#a6b05e6c88fcca31b2e32d3ae5162ef52", null ],
    [ "bTerminatorTlvPresence", "dc/d03/structphalTop__T1T__t.html#a180367b9ac447f4fa3db7a57e5ab5c8d", null ],
    [ "bLockTlvCount", "dc/d03/structphalTop__T1T__t.html#aec81a0b777b7ee0898c6dd24b198007c", null ],
    [ "bMemoryTlvCount", "dc/d03/structphalTop__T1T__t.html#a3f93eabade8709c0ff57091efe215c5f", null ],
    [ "wNdefHeaderAddr", "dc/d03/structphalTop__T1T__t.html#a8b5f58ebe39b8be13a48c83fd84be132", null ],
    [ "wNdefMsgAddr", "dc/d03/structphalTop__T1T__t.html#a4dcf93bfa216e5fc26e8210f8347fa36", null ],
    [ "bUid", "dc/d03/structphalTop__T1T__t.html#a5dcab06c5a157c9fee4c944f6037774c", null ],
    [ "asMemCtrlTlv", "dc/d03/structphalTop__T1T__t.html#a0f958666448ed38e80cd7fe6a09ffedb", null ],
    [ "asLockCtrlTlv", "dc/d03/structphalTop__T1T__t.html#a459e29d121929b5ca56eead09101068f", null ],
    [ "sSegment", "dc/d03/structphalTop__T1T__t.html#ab647a25dafa49fd2659abd8204b9ab00", null ]
];
var structphhalHw__InstMngr__CmdParams__t =
[
    [ "bCmd", "d2/d3f/structphhalHw__InstMngr__CmdParams__t.html#a38ce9be2c9ae68c43c0ec93e6371cba1", null ],
    [ "bQueue", "d2/d3f/structphhalHw__InstMngr__CmdParams__t.html#acb7fa044123f5fee3c96399738bde5a3", null ],
    [ "pTxDataBuff", "d2/d3f/structphhalHw__InstMngr__CmdParams__t.html#a05e757fb3b2eec2b96b689cb53631b12", null ],
    [ "wTxDataLength", "d2/d3f/structphhalHw__InstMngr__CmdParams__t.html#a0fc9ec99369e3278c06e7fc233719191", null ],
    [ "pAddnData", "d2/d3f/structphhalHw__InstMngr__CmdParams__t.html#ad1ed3693e470712723ad3e2da5165049", null ],
    [ "wAddnDataLen", "d2/d3f/structphhalHw__InstMngr__CmdParams__t.html#a0c364d6ef54675db425ca7152432cfdf", null ],
    [ "ppRxBuffer", "d2/d3f/structphhalHw__InstMngr__CmdParams__t.html#a38c863f925a45ac0a26e8ec6893e8dc3", null ],
    [ "pRxLength", "d2/d3f/structphhalHw__InstMngr__CmdParams__t.html#aac9a30e4d970bd24944ad3391256c5c3", null ]
];
var group__phalMfp__Sw =
[
    [ "phalMfp_Sw_DataParams_t", "d8/d72/structphalMfp__Sw__DataParams__t.html", [
      [ "wId", "d8/d72/structphalMfp__Sw__DataParams__t.html#ab8ef7c65bf98012adbf486911765625f", null ],
      [ "pPalMifareDataParams", "d8/d72/structphalMfp__Sw__DataParams__t.html#a5b15070d0e67fa1c6827f702e66b818c", null ],
      [ "pKeyStoreDataParams", "d8/d72/structphalMfp__Sw__DataParams__t.html#a75b6da86d2ac63c0c91a391c60b69977", null ],
      [ "pCryptoDataParamsEnc", "d8/d72/structphalMfp__Sw__DataParams__t.html#a2d04a861da619c8806b0b8ab455232fc", null ],
      [ "pCryptoDataParamsMac", "d8/d72/structphalMfp__Sw__DataParams__t.html#a12a44240645ef7ba7a726b189ff92ca8", null ],
      [ "pCryptoRngDataParams", "d8/d72/structphalMfp__Sw__DataParams__t.html#a188156a9282daf7aea697dd73fc83bc9", null ],
      [ "pCryptoDiversifyDataParams", "d8/d72/structphalMfp__Sw__DataParams__t.html#a02cf09e74abb73b317698bb09866323c", null ],
      [ "bKeyModifier", "d8/d72/structphalMfp__Sw__DataParams__t.html#a5f11716ea793771d30c3068371a8dcde", null ],
      [ "wRCtr", "d8/d72/structphalMfp__Sw__DataParams__t.html#ae1fd15bd80b567ccd9e10d0499a03dcf", null ],
      [ "wWCtr", "d8/d72/structphalMfp__Sw__DataParams__t.html#adc8a27b7a7e9720d470e0b4b40ee3874", null ],
      [ "bTi", "d8/d72/structphalMfp__Sw__DataParams__t.html#a634821f832b4dd1d3f5827c61b83a230", null ],
      [ "bNumUnprocessedReadMacBytes", "d8/d72/structphalMfp__Sw__DataParams__t.html#a2487829689410c4c47d5c0dcd4c71c65", null ],
      [ "pUnprocessedReadMacBuffer", "d8/d72/structphalMfp__Sw__DataParams__t.html#a8dc66de291e201f5a992d1f60332411a", null ],
      [ "pIntermediateMac", "d8/d72/structphalMfp__Sw__DataParams__t.html#a0be5d591d9c0d92309e0d445d9b37f08", null ],
      [ "bFirstRead", "d8/d72/structphalMfp__Sw__DataParams__t.html#ad1e3bc5571d0001badb41935ac6c4f06", null ]
    ] ],
    [ "PHAL_MFP_SW_ID", "dd/d0d/group__phalMfp__Sw.html#ga0116c4a8406da0e136db1168d7aaf8e0", null ],
    [ "phalMfp_Sw_Init", "dd/d0d/group__phalMfp__Sw.html#ga3b570dcffbf2337c71490e47559e314b", null ]
];
var structphalTop__Sw__DataParams__t =
[
    [ "wId", "da/ded/structphalTop__Sw__DataParams__t.html#a92883f700fdc13962a398cf3f862ef4f", null ],
    [ "bTagType", "da/ded/structphalTop__Sw__DataParams__t.html#a2dcc6cb9e10c275ebe76c019c975472c", null ],
    [ "bVno", "da/ded/structphalTop__Sw__DataParams__t.html#afda57ce1c16346e571838065fcfdbaf8", null ],
    [ "bTagState", "da/ded/structphalTop__Sw__DataParams__t.html#a493b602e7dde8c0068f98b0f0da36ea6", null ],
    [ "wNdefLength", "da/ded/structphalTop__Sw__DataParams__t.html#a38314d5452faf42fb47029be9750f024", null ],
    [ "wMaxNdefLength", "da/ded/structphalTop__Sw__DataParams__t.html#a9eeaad70bc49797b2e24e4d27c588440", null ],
    [ "pTopTagsDataParams", "da/ded/structphalTop__Sw__DataParams__t.html#a4280a4e499248ddda335295a6ed810bb", null ],
    [ "salTop_T1T", "da/ded/structphalTop__Sw__DataParams__t.html#a4c2fe911a779325fb7cda6b28cdfce49", null ],
    [ "salTop_T2T", "da/ded/structphalTop__Sw__DataParams__t.html#ab1ef29d6e6f505b2de0f7aa9002dfbed", null ],
    [ "salTop_T3T", "da/ded/structphalTop__Sw__DataParams__t.html#a924fe599742c4335c794c3642648916a", null ],
    [ "salTop_T4T", "da/ded/structphalTop__Sw__DataParams__t.html#a9a780abd675bee91272cf81399b3c7f1", null ],
    [ "salTop_T5T", "da/ded/structphalTop__Sw__DataParams__t.html#a3602685432378bcf3192d4138ef3f902", null ],
    [ "salTop_MfcTop", "da/ded/structphalTop__Sw__DataParams__t.html#a329dc24f9c56f2e803081ec5fd9e757f", null ]
];
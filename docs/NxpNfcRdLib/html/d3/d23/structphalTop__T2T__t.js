var structphalTop__T2T__t =
[
    [ "pAlT2TDataParams", "d3/d23/structphalTop__T2T__t.html#ae02f53fc4a1d442422e2832f0822aa41", null ],
    [ "bRwa", "d3/d23/structphalTop__T2T__t.html#ae1f7c592bc94fad34fc741397b2d3ee9", null ],
    [ "bTms", "d3/d23/structphalTop__T2T__t.html#a15be3399e0d591f6a42f278a834d9582", null ],
    [ "bTagMemoryType", "d3/d23/structphalTop__T2T__t.html#a7a8a74857fff4773b5250aceecee54dc", null ],
    [ "bLockTlvCount", "d3/d23/structphalTop__T2T__t.html#a61ae168d4b695b8f84751162b093bd0f", null ],
    [ "bMemoryTlvCount", "d3/d23/structphalTop__T2T__t.html#a700fe640358f8aafb08bee63717a2eb0", null ],
    [ "wNdefHeaderAddr", "d3/d23/structphalTop__T2T__t.html#a5de7dd838ba16373da444ef2634fec24", null ],
    [ "wNdefMsgAddr", "d3/d23/structphalTop__T2T__t.html#ac23daa62e744ddc8740218402e1e20db", null ],
    [ "asMemCtrlTlv", "d3/d23/structphalTop__T2T__t.html#ae6fcdfb75a87d5c224b8ca77dcef4c35", null ],
    [ "asLockCtrlTlv", "d3/d23/structphalTop__T2T__t.html#a128438768986161d44105de0f9a474d2", null ],
    [ "sSector", "d3/d23/structphalTop__T2T__t.html#a58a40e2d710b5430feab415ed417d702", null ]
];
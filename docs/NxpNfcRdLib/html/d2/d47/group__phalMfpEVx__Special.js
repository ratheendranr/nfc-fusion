var group__phalMfpEVx__Special =
[
    [ "PHAL_MFPEVX_UID_TYPE_UIDF0", "d2/d47/group__phalMfpEVx__Special.html#ga5dcc86dc224c29f038bf0971e4c924d6", null ],
    [ "PHAL_MFPEVX_UID_TYPE_UIDF1", "d2/d47/group__phalMfpEVx__Special.html#ga1b23090750be1c76348340773df13846", null ],
    [ "PHAL_MFPEVX_UID_TYPE_UIDF2", "d2/d47/group__phalMfpEVx__Special.html#ga4faa831f79c9a5b732c8326257f0c613", null ],
    [ "PHAL_MFPEVX_UID_TYPE_UIDF3", "d2/d47/group__phalMfpEVx__Special.html#ga18c2d420ff6214464590d856e7dcb8d1", null ],
    [ "PHAL_MFPEVX_ENABLE_ISO14443_L4", "d2/d47/group__phalMfpEVx__Special.html#ga3f51f039c79f12dd98bdbc21f111580d", null ],
    [ "PHAL_MFPEVX_DISABLE_ISO14443_L4", "d2/d47/group__phalMfpEVx__Special.html#ga706ca0cf6c004624a366497d300229eb", null ],
    [ "phalMfpEVx_GetVersion", "d2/d47/group__phalMfpEVx__Special.html#ga2e2a0e638dda6359efeb44a61927db9f", null ],
    [ "phalMfpEVx_ReadSign", "d2/d47/group__phalMfpEVx__Special.html#ga2b32a98a78c42a5acb98074f02bbbad5", null ],
    [ "phalMfpEVx_ResetAuth", "d2/d47/group__phalMfpEVx__Special.html#ga7d90c2a5f088b881e8e1977ed280f248", null ],
    [ "phalMfpEVx_PersonalizeUid", "d2/d47/group__phalMfpEVx__Special.html#ga4e781bf24c109e0f347ce9e1c07e3152", null ],
    [ "phalMfpEVx_SetConfigSL1", "d2/d47/group__phalMfpEVx__Special.html#gabe081c9553a91911bdc27d92c5edda17", null ],
    [ "phalMfpEVx_ReadSL1TMBlock", "d2/d47/group__phalMfpEVx__Special.html#ga54234f55dbcc7c394774e7b5cf30dc7c", null ],
    [ "phalMfpEVx_VCSupportLastISOL3", "d2/d47/group__phalMfpEVx__Special.html#gac2a061a54575e028ad26b6150326a584", null ]
];
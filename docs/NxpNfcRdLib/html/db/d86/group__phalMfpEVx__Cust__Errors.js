var group__phalMfpEVx__Cust__Errors =
[
    [ "PHAL_MFPEVX_ERR_AUTH", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga0ff54df88c6ce99ce31be20650da5ab2", null ],
    [ "PHAL_MFPEVX_ERR_CMD_OVERFLOW", "db/d86/group__phalMfpEVx__Cust__Errors.html#gad66d24a2b51718d3fda3664404eb6b2b", null ],
    [ "PHAL_MFPEVX_ERR_MAC_PCD", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga422289df5a4cba9c63f26d3ec1334584", null ],
    [ "PHAL_MFPEVX_ERR_BNR", "db/d86/group__phalMfpEVx__Cust__Errors.html#gab78381d713816101304146de2d6ff1ce", null ],
    [ "PHAL_MFPEVX_ERR_EXT", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga98974c79d0a5a524023f75df8451a227", null ],
    [ "PHAL_MFPEVX_ERR_CMD_INVALID", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga062b0b86b6365bfcf932d54514dd9fcf", null ],
    [ "PHAL_MFPEVX_ERR_FORMAT", "db/d86/group__phalMfpEVx__Cust__Errors.html#gad171c49f90f82705c1e70ea58aebb9e7", null ],
    [ "PHAL_MFPEVX_ERR_GEN_FAILURE", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga0167fa01dac02d82890b03c4dc8ab9ad", null ],
    [ "PHAL_MFPEVX_ERR_TM", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga220854643c874be525b5a1c1853bc436", null ],
    [ "PHAL_MFPEVX_ERR_NOT_SUP", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga45d3b14c193eb92756d2f32a17075b3e", null ],
    [ "PHAL_MFPEVX_ISO7816_ERR_WRONG_LENGTH", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga300020397d434d022a4ae06adeb343b7", null ],
    [ "PHAL_MFPEVX_ISO7816_ERR_WRONG_PARAMS", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga1c1890ae175eb96ff52d9d8fa2e60cb3", null ],
    [ "PHAL_MFPEVX_ISO7816_ERR_WRONG_LC", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga87c01b2f526b35c5d788b500137cd4e0", null ],
    [ "PHAL_MFPEVX_ISO7816_ERR_WRONG_LE", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga18b7dd850fa126d94145199f4570d80a", null ],
    [ "PHAL_MFPEVX_ISO7816_ERR_WRONG_CLA", "db/d86/group__phalMfpEVx__Cust__Errors.html#ga0a6c112b870bbcb87145ff4e00e4425d", null ]
];
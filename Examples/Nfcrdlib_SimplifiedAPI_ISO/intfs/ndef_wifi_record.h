/*
 * ndef_wifi_record.h
 *
 *  Created on: Apr. 10, 2021
 *      Author: pankil
 */

#ifndef NDEF_WIFI_RECORD_H_
#define NDEF_WIFI_RECORD_H_

#include "ndef_msg.h"

#define 	SSID_FIELD_ID			0x1045
#define	NETWORK_KEY_FIELD_ID		0x1027
#define 	MAC_ADDRESS_ID 			0x1020
#define 	AUTH_TYPE_FIELD_ID	   	0x1003
#define 	ENCRYPTION_TYPE_ID      0x100F
#define 	AUTH_TYPE_OPEN          0x0001
#define 	AUTH_TYPE_WPA_PSK       0x0002
#define 	AUTH_TYPE_WPA_EAP       0x0008
#define 	AUTH_TYPE_WPA2_EAP      0x0010
#define 	AUTH_TYPE_WPA2_PSK      0x0020
#define 	ENCRYPT_TYPE_NONE       0x0001
#define 	ENCRYPT_TYPE_WEP        0x0002
#define 	ENCRYPT_TYPE_TKIP       0x0004
#define 	ENCRYPT_TYPE_AES        0x0008
#define 	ENCRYPT_TYPE_MMODE      0x000c   //AES/TKIP Mixed Mode

typedef struct{
	char 	ssid[32];
	char 	password[32];
	uint8_t	mac_addr[6];
	char	auth_type[12];
	char	encryption_type[32];
}wifi_record_t;

uint32_t NfcProcessWifiRecord(ndef_record_t *record);

#endif //NDEF_WIFI_RECORD_H_
/*
 * ndef_wifi_record.c
 *
 *  Created on: Apr. 10, 2021
 *      Author: pankil
 */

#include "ndef_wifi_record.h"
#include "ndef_print.h"
#include <stdlib.h>

uint32_t NfcProcessWifiRecord(ndef_record_t *record)
{
	wifi_record_t wifi_record;
	char string[256];
	
	DEBUG_PRINTF("\n Decoding wifi record");

	if(record == NULL)
	{
		DEBUG_PRINTF("ERR: NULL DATA");
	}

		
	uint16_t wifi_field_id = 0;
	uint16_t wifi_field_len = 0;
		
	for(int i=3; i<record->payload_length; i++)
	{
		
		wifi_field_id = (record->record_payload.record_value[i-3]<<8 | record->record_payload.record_value[i-2]);
		wifi_field_len = (record->record_payload.record_value[i-1]<<8 | record->record_payload.record_value[i]);
			
		if(wifi_field_id == SSID_FIELD_ID)
		{
			if(wifi_field_len)
			{
				//uint8_t wifi_ssid[wifi_field_len];
				memcpy(&wifi_record.ssid[0], (char*)record->record_payload.record_value+i+1, wifi_field_len);
				wifi_record.ssid[wifi_field_len] = '\0';
				DEBUG_PRINTF("\n SSID : %s", wifi_record.ssid);	
			}
		}
		else if (wifi_field_id == NETWORK_KEY_FIELD_ID) 
		{
			if (wifi_field_len) 
            {
            	memcpy(&wifi_record.password[0], (char*)record->record_payload.record_value+i+1, wifi_field_len);
            	wifi_record.password[wifi_field_len] = '\0';
               DEBUG_PRINTF("\n PASSWORD : %s", wifi_record.password);
            }
		}
		else if (wifi_field_id == MAC_ADDRESS_ID) 
		{
			if (wifi_field_len) 
            {
             	memcpy(&wifi_record.mac_addr[0], record->record_payload.record_value+i+1, wifi_field_len);
               DEBUG_PRINTF("\n MAC ADDRESS :");
					phApp_Print_Buff(&wifi_record.mac_addr[0], 6);
            }
       	}
          
        else if (wifi_field_id == AUTH_TYPE_FIELD_ID) 
        {
        		DEBUG_PRINTF("\n Authentication type :");
            // Use wifi_field_id to capture the Auth Type 16bit value
            if (wifi_field_len == 2) 
            {
                wifi_field_id = (record->record_payload.record_value[i+1]<< 8) | record->record_payload.record_value[i+2];
                if (wifi_field_id & AUTH_TYPE_WPA_PSK || wifi_field_id & AUTH_TYPE_WPA2_PSK) 
                {
                 	DEBUG_PRINTF(" WPA_PSK");
                } 
                else if (wifi_field_id & AUTH_TYPE_WPA_EAP || wifi_field_id & AUTH_TYPE_WPA2_EAP) 
                {
                    DEBUG_PRINTF(" WPA_EAP");
                } 
                else if (wifi_field_id & AUTH_TYPE_OPEN) 
                {
                    DEBUG_PRINTF(" NONE (Open)");
                }
                else 
                {
                 	phApp_Print_Buff(&record->record_payload.record_value[i+1], 2);
                }
            }
        }
        else if (wifi_field_id == ENCRYPTION_TYPE_ID) 
        {
        	if (wifi_field_len == 2) 
            {
            	DEBUG_PRINTF("\n Encryption type :");
              	wifi_field_id = (record->record_payload.record_value[i+1]<< 8) | record->record_payload.record_value[i+2];
                if (wifi_field_id == ENCRYPT_TYPE_NONE)
                {
                	DEBUG_PRINTF(" No Encryption Type");
                }
                else if (wifi_field_id == ENCRYPT_TYPE_WEP) 
                {
                	DEBUG_PRINTF(" WEP Encryption Type - deprecated");
                }
                else if (wifi_field_id == ENCRYPT_TYPE_TKIP) 
                {
                	DEBUG_PRINTF(" TKIP Encryption Type");
                }
                else if (wifi_field_id == ENCRYPT_TYPE_AES)
                { 
                	DEBUG_PRINTF(" AES Encryption Type");
               	}
                else if (wifi_field_id == ENCRYPT_TYPE_MMODE)
                {
                	DEBUG_PRINTF(" AES/TKIP Mixed Mode Type");
                }
            }
        }     
    }


    //Temp implementation
    //Connect to wifi
	 sprintf(string, "wpa_passphrase %s %s | sudo tee /etc/wpa_supplicant.config", wifi_record.ssid, wifi_record.password);
	 //DEBUG_PRINTF("\n\n\n\n%s\n", string);
	 system(string);
	 system("sudo wpa_supplicant -c /etc/wpa_supplicant.config -i wlan0");   
}
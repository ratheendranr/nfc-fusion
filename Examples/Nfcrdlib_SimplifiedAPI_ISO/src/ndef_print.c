/*
 * ndef_print.c
 *
 *  Created on: Apr. 10, 2021
 *      Author: pankil
 */

#include <phApp_Init.h>
#include "ndef_print.h"


void PrintHex(const uint8_t * data, const long numBytes)
{
  int32_t szPos;
  for (szPos=0; szPos < numBytes; szPos++)
  {
    DEBUG_PRINTF("0x");
    // Append leading 0 for small values
    if (data[szPos] <= 0xF)
    {
      DEBUG_PRINTF("0");
   }
    DEBUG_PRINTF("%2X", data[szPos]&0xff);
    if ((numBytes > 1) && (szPos != numBytes - 1))
    {
      DEBUG_PRINTF(" ");
    }
  }
  DEBUG_PRINTF("\n");
}



void PrintHexChar(const uint8_t * data, const long numBytes)
{
  int32_t szPos;
  DEBUG_PRINTF("  ");
  for (szPos=0; szPos < numBytes; szPos++)
  {
    if (data[szPos] <= 0x1F)
    {
      DEBUG_PRINTF(".");
   }
    else
    {
      DEBUG_PRINTF("%c", (char)data[szPos]);
   }
  }
  DEBUG_PRINTF("\n");
}


void DumpHex(const uint8_t * data, const long numBytes, const unsigned int blockSize)
{
    int i;
    for (i = 0; i < (numBytes / blockSize); i++)
    {
        PrintHexChar(data, blockSize);
        data += blockSize;
    }
}


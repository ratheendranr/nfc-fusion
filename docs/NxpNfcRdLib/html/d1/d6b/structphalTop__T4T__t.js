var structphalTop__T4T__t =
[
    [ "pAlT4TDataParams", "d1/d6b/structphalTop__T4T__t.html#aa9e95372693c9a7ef009e6530b235267", null ],
    [ "aNdefFileID", "d1/d6b/structphalTop__T4T__t.html#a7ec07f3bf3706b4c5aa85d1eec4d4eea", null ],
    [ "bRa", "d1/d6b/structphalTop__T4T__t.html#a91598212a9f051e28dbbe16a0d8c0e7f", null ],
    [ "bWa", "d1/d6b/structphalTop__T4T__t.html#a06ebea5cd47b0c5a220271ff5c38d243", null ],
    [ "bCurrentSelectedFile", "d1/d6b/structphalTop__T4T__t.html#aabd7a4f7b638c173d520d5c9dfab89d6", null ],
    [ "wMLe", "d1/d6b/structphalTop__T4T__t.html#abb05ccfdd0b9082533804db7c6973d12", null ],
    [ "wMLc", "d1/d6b/structphalTop__T4T__t.html#ac3666b57768ff1c8473ad01f5bf781c2", null ],
    [ "wCCLEN", "d1/d6b/structphalTop__T4T__t.html#ae9346092dd490c9a5e5aba179cfcf7a0", null ],
    [ "wMaxFileSize", "d1/d6b/structphalTop__T4T__t.html#a8e80e6a0d697fb71838b3453ea49fd2f", null ]
];
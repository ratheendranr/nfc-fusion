var group__phalMfpEVx__Sw =
[
    [ "phalMfpEVx_Sw_DataParams_t", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html", [
      [ "wId", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a291fe4aff23b57c842e1e80f34bc8a30", null ],
      [ "pPalMifareDataParams", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a68eb7a2fa6f7f10b0684bada11d1f729", null ],
      [ "pKeyStoreDataParams", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a93924eb7ac83f995c1464fe5ab71b9d7", null ],
      [ "pCryptoDataParamsEnc", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a48682c6153c657a0f93f512a81ac2505", null ],
      [ "pCryptoDataParamsMac", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a968eebad806f5383a6923672f8049c49", null ],
      [ "pCryptoRngDataParams", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a6a2ccd4a29a57931bc93212bfaaa3628", null ],
      [ "pCryptoDiversifyDataParams", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#aa6e2d280b743864ff9df3036f2413b69", null ],
      [ "pTMIDataParams", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a77071e82ed95faad6bad198469e13659", null ],
      [ "pVCADataParams", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#ac30f4f5deb12fa466bc8de59c5197af2", null ],
      [ "wRCtr", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a288418cb9e17d77d7e3c20d0eca3eecc", null ],
      [ "wWCtr", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#ab1b70793be5ce490b44e1a457810a1f5", null ],
      [ "bWrappedMode", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#af3895990461825b4d68aa4a8f18662af", null ],
      [ "bExtendedLenApdu", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a0e8acce10492d693add036d207787f4c", null ],
      [ "bTi", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#aaf7878c2b9b2040e88d0c71455dc3b0e", null ],
      [ "bNumUnprocessedReadMacBytes", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#ac3bc3a1bbbcdbc36eefb2ee868ca2b90", null ],
      [ "pUnprocessedReadMacBuffer", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a20459b596a41c4f1850fb4ce731bb447", null ],
      [ "pIntermediateMac", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a88728d3ae63ad0fbdc81ed8352120ccc", null ],
      [ "bFirstRead", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a7793932c41fe4f812bcc8c02a0679f2b", null ],
      [ "bIv", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a8a71475fd8895b647d28ccc0ba0cf13d", null ],
      [ "bSesAuthENCKey", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#ad0b515e21627a47c61c32d9858c4d6d5", null ],
      [ "bSesAuthMACKey", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a1448fd2e1550d9b646bfbf7b5e73ccf7", null ],
      [ "bAuthMode", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#a1bbcecbe1e96d4321e310333d5bc27da", null ],
      [ "bSMMode", "d5/d4c/structphalMfpEVx__Sw__DataParams__t.html#acd0054cc15fc7624c44bd36246c7334f", null ]
    ] ],
    [ "phalMfpEVx_Sw_Init", "d6/df1/group__phalMfpEVx__Sw.html#gab8700018ac49a8efdc0eee2245836d8e", null ]
];